import requests
import time
from gameConfig import main as Game
from market.ticker import main as Ticker
import redis,pickle
from flask import Flask, request
import json
import threading

r = redis.StrictRedis(host='localhost', port=6380, db=15)

app  = Flask(__name__)

def initialize():
    for company in Game.companies():
        ticker = Ticker.getTickerPrice(company=company)
        r.set(company, pickle.dumps([ticker for x in range(5)]))

@app.route('/tickerRecord')
def getTickerRecord():
    obj = {}
    for company in Game.companies():
        ticker = pickle.loads(r.get(company))
        obj[company] = ticker
    return json.dumps(obj)

def listen():
    while True:
        print('tickerListeneer:: woke up')
        for company in Game.companies():
            try:
                req = requests.get('http://localhost:5003/ticker?company=' + company)
            except Exception:
                req = None
            if req != None:
                existing = pickle.loads(r.get(company))
                existing.append(float(req.content.decode('utf-8')))
                if len(existing) > 5:
                    existing = existing[1:]
                r.set(company, pickle.dumps(existing))
        print('tickerListener:: going to sleep')
        time.sleep(60*5)



if __name__ == '__main__':
    print('tickerListener started!')
    TEST = False
    if TEST == True:
        initialize()
    listenThread = threading.Thread(target=listen,args=())
    listenThread.start()
    app.run(host='localhost',port=5009,threaded=True)
