import jwt

def generateToken(payload):
    encoded = (jwt.encode({'details': payload}, 'SpaceSex', algorithm='HS256')).decode('utf-8')
    return encoded

def decodeToken(token):
    decoded = jwt.decode(token, 'SpaceSex', algorithms=['HS256'])
    return decoded
