import redis
import uuid
import pickle
from gameConfig import main as gConf
import math
import unittest
from itertools import groupby
from operator import itemgetter,attrgetter
from order import main as Order
import collections
import market.ticker.main as Ticker
import hashlib
from mail import main as Mail


'''
User:
    name:
    username:
    mobile:
    kid:
    email:
    password:
    cash: ->Integer
    stock: ->Dict
        company: Units->Integer
    orders: -> Array
    id: ->String
'''

DUPLICATE_USERNAME = -1

def setConnection(db=1):
    r = redis.StrictRedis(host='localhost', port=6379, db=db)
    return r


def creditAndDecreaseStock(userId, credit, stockSymbol, units,r):
    user = getUser(userId,r=r)
    user['cash'] += credit
    user['stock'][stockSymbol] -= units
    updateUser(userId, obj=user,r=r)
    return 1

def creditDueAndIncreaseStock(userId,due,stockSymbol,units,r):
    user = getUser(userId,r=r)
    user['cash'] += due
    user['stock'][stockSymbol] += units
    updateUser(userId,obj=user,r=r)
    return 1

def debitAndIncreaseStock(userId, debit, stockSymbol, units,r):
    user = getUser(userId,r=r)
    user['cash'] -= credit
    user['stock'][stockSymbol] += units
    updateUser(userId, obj=user,r=r)
    return 1

def maximumPossibleUnits(userId,unitPrice,r):
    user = getUser(userId,r=r)
    units = math.floor(user['cash']/unitPrice)
    return units

def credit(amount,userId,r):
    user = getUser(userId, r=r)
    user['cash'] += amount
    updateUser(userId, obj=user, r=r)
    return 1

def increaseStock(units, company, userId, r):
    user = getUser(userId, r=r)
    user['stock'][company] += units
    updateUser(userId, obj=user, r=r)
    return 1

def decreaseStock(units, company, userId, r):
    user = getUser(userId, r=r)
    user['stock'][company] -= units
    updateUser(userId, obj=user, r=r)
    return 1

def hasHeOrSheEnoughCashForStocks(userId, amount,r):
    user = getUser(userId,r=r)
    return user['cash'] >= amount

def debitMaxLimit(userId, amount,r):
    user = getUser(userId,r=r)
    if user['cash'] >= amount:
        user['cash'] -= amount
        updateUser(userId,obj=user,r=r)
        return 1
    else:
        return 0

def wipeClean(r):
    r.flushdb()

def generateId():
    return str(uuid.uuid1()) # UUID -> Primary Key

def getAllUser(r):
    array = []
    obj = r.hgetall('user')
    for key in obj.keys():
        array.append(pickle.loads(obj[key]))
    return array

def getUser(userId, r):
    value = r.hget('user', userId)
    if value == None:
        return -1
    else:
        data = pickle.loads(value)
        orders = data['orders']
        for order in orders:
            order['limit'] = (order['limit'])
        data['orders'] = orders
        return data


def updateUser(userId, obj, r):
    r.hset('user', userId, pickle.dumps(obj))
    return 1

def updateUserOrder(company,units,status,orderId,time,type,userId,r,executedPrice='',totalUnits='',limit=''):
    user = getUser(userId,r)
    #print('updateUserOrder {}'.format(orderId))
    #print('updateUserOrder All {}'.format(Order.get()))
    orderDetailsFromOrderDB = Order.getById(id=orderId)
    if orderDetailsFromOrderDB != None:
        total = orderDetailsFromOrderDB['units']
        limit = orderDetailsFromOrderDB['limit']
    else:
        total = (totalUnits)
        limit = (limit)
    orderDetails = {
        'company':company,
        'units':units,
        'status':status,
        'orderId':orderId,
        'time':time,
        'type':type,
        'totalUnits':total,
        'limit':limit,
        'cancel': False
    }
    if status in ['executed','partially executed']:
        orderDetails['executedPrice'] = executedPrice
    if status in ['partially executed', 'added']:
        orderDetails['cancel'] = True
    user['orders'].append(orderDetails)
    updateUser(userId,obj=user,r=r)
    return 1

#Creating a new user
def createNewUser(user, r):
    obj = user
    userlists = getAllUser(r)
    for userlist in userlists:
        if (userlist['username'] == obj['username']):
            return DUPLICATE_USERNAME
        if(userlist['email'] == obj['email']):
            return -2
        if(userlist['mobile'] == obj['mobile']):
            return -3
    userId =  generateId()
    obj['registered'] = False
    obj['cId'] = str(hashlib.sha256(str(uuid.uuid4()).encode('utf-8')).hexdigest())
    obj['id'] = userId
    obj['cash'] = gConf.defaultUserCash()
    obj['stock'] = {}
    obj['orders'] = []
    for company in gConf.companies():
        obj['stock'][company] = 0

    r.hset('user', userId, pickle.dumps(obj))
    return obj['cId']



def confirmUser(userCId, r):
    users = getAllUser(r=r)
    userWithCID = list(filter(lambda user: user['cId'] == userCId, users))
    if len(userWithCID) == 0:
        return -1
    elif userWithCID[0]['registered'] ==  True:
        return 2
    else:
        userWithCID[0]['registered'] = True
        updateUser(userId=userWithCID[0]['id'], obj=userWithCID[0], r=r)
        return 1

def forgotPassword(userEmail, r):
    users = getAllUser(r=r)
    userWithThisEmail = list(filter(lambda user: user['email'] == userEmail, users))
    if len(userWithThisEmail) == 0:
        return -1
    else:
        userWithThisEmail[0]['forgotPassword'] = True
        userWithThisEmail[0]['forgotPasswordLink'] = str(hashlib.sha256(str(uuid.uuid4()).encode('utf-8')).hexdigest())
        updateUser(userId=userWithThisEmail[0]['id'], obj=userWithThisEmail[0], r=r)
        Mail.sendForgotPasswordLink(mailId=userWithThisEmail[0]['email'], forgotPasswordLink= userWithThisEmail[0]['forgotPasswordLink'])
        return 1

def changeForgotPassword(forgotPasswordLink, password, r):
    users = getAllUser(r=r)
    userWithThisLink = list(filter(lambda user: 'forgotPasswordLink' in user.keys() and user['forgotPasswordLink'] == forgotPasswordLink, users))
    if len(userWithThisLink) == 0:
        return -1
    else:
        if userWithThisLink[0]['forgotPassword'] == None or userWithThisLink[0]['forgotPassword'] == False:
            return -2
        else:
            userWithThisLink[0]['password'] = password
            userWithThisLink[0]['forgotPassword'] = False
            updateUser(userId=userWithThisLink[0]['id'], obj=userWithThisLink[0], r=r)
            print('change forgot password {}'.format(userWithThisLink[0]))
            return 1



def orderInformation(userId, r):
    users = getAllUser(r)
    filtered = list(filter(lambda user: user['id'] == userId, users))[0]
    sortedByTime = sorted(filtered['orders'],key=itemgetter('time'),reverse=True) # Order status in Descending order
    orders = {}
    for key,group in groupby(sortedByTime, lambda z: z['orderId']):
        orders[key] = []
    for key,group in groupby(sortedByTime, lambda z: z['orderId']):
        for thing in group:
            orders[key].append(thing)

    sortedOrders = collections.OrderedDict(sorted(orders.items(), key = lambda x: x[1][0]['time'],reverse=True)) # Orders in Descending Order

    return sortedOrders

def data(userId, r):
    users = getAllUser(r)
    filtered = list(filter(lambda user: user['id'] == userId, users))[0]
    del filtered['password']
    return filtered

def login(username, password, r):
    users = getAllUser(r)
    filtered = list(filter(lambda user:  user['username'] == username and user['password'] == password, users))
    if len(filtered) == 1:
        if filtered[0]['registered'] ==  True:
            return filtered[0]['id']
        else:
            return -2
    else:
        return -1

def rank(r, userId, top=10):
    users = getAllUser(r=r)
    for user in users:
        stockValue = 0
        user['totalCash'] = user['cash']
        for company in gConf.companies():
            specificStockVal = user['stock'][company] * Ticker.getTickerPrice(company=company)
            stockValue += specificStockVal
        user['totalCash'] += stockValue
    sortedByCash = sorted(users,key=lambda x: x['totalCash'],reverse=True)
    rank=1
    cash = 0
    totalCash = 0
    userRank = 0
    obj = {}
    for user in sortedByCash:
        user['rank'] = rank
        if userId == user['id']:
            userRank = rank
            totalCash = user['totalCash']
            cash = user['cash']
        rank = rank+1
    filteredSorted = list(map(lambda x: {'username':x['username'],'rank':x['rank'], 'cash':x['cash'], 'totalCash':x['totalCash']}, sortedByCash))
    obj['others'] = filteredSorted[:top]
    obj['user'] = {
        'rank':userRank,
        'cash': cash,
        'totalCash': totalCash
    }
    return obj

def getTotalNumberOfUsers(r):
    users =  getAllUser(r=r)
    if users != None:
        return len(users)
    else:
        return -1

def creditUserByAdmin(username, cash, r):
    users = getAllUser(r=r)
    userToBeCredited = list(filter(lambda x: x['username'] == username, users))
    if len(userToBeCredited)>0:
        userToBeCredited[0]['cash'] += int(cash)
        updateUser(userId=userToBeCredited[0]['id'],obj=userToBeCredited[0],r=r)
        return 1
    else:
        return -1

def debitUserByAdmin(username, cash, r):
    users = getAllUser(r=r)
    userToBeCredited = list(filter(lambda x: x['username'] == username, users))
    if len(userToBeCredited) > 0:
        userToBeCredited[0]['cash'] -= int(cash)
        updateUser(userId=userToBeCredited[0]['id'], obj=userToBeCredited[0], r=r)
        return 1
    else:
        return -1

class tests(unittest.TestCase):
    def setUp(self):
        print('started testing')
        global r
        r = setConnection(db=13)
        wipeClean(r)
    def testCreateNewUser(self):
        user = {
            'name': 'kandhan',
            'username': 'kk',
            'password': '123',
            'email': 'k@k.com',
            'mobile': '8903121126',
            'kid': 'k123'
        }
        createNewUser(user, r)
        userId = login(user['username'], user['password'], r)
        self.assertEqual(type(userId),type('string'))
        user = getUser(userId,r)
        self.assertEqual(user['name'], 'kandhan')

    def testDuplicateUserCreation(self):
        user = {
            'name': 'kandhan',
            'username': 'kk',
            'password': '123',
            'email': 'k@k.com',
            'mobile': '8903121126',
            'kid': 'k123'
        }
        createNewUser(user, r)
        user = {
            'name': 'kandhan',
            'username': 'kk',
            'password': '123',
            'email': 'k@k.com',
            'mobile': '8903121126',
            'kid': 'k123'
        }
        self.assertEqual(createNewUser(user, r),DUPLICATE_USERNAME)
    def testMaxPossibleUnits(self):
        user = {
            'name': 'kandhan',
            'username': 'kk',
            'password': '123'
        }
        createNewUser(user, r)
        userId = login(user['username'], user['password'], r)
        user = getUser(userId,r)
        self.assertEqual(maximumPossibleUnits(userId,unitPrice=15,r=r),666)
    def testEnoughCash(self):
        user = {
            'name': 'kandhan',
            'username': 'kk1',
            'password': '123'
        }
        print(createNewUser(user, r))
        userId = login(user['username'], user['password'], r)
        print(userId)
        self.assertEqual(hasHeOrSheEnoughCashForStocks(userId=str(userId),amount=1500,r=r), True)
        self.assertEqual(hasHeOrSheEnoughCashForStocks(userId=str(userId), amount=1000, r=r), True)
    def testRank(self):
        user = {
            'name': 'kandhan',
            'username': 'kk',
            'password': '123',
            'email': 'k@k.com',
            'mobile': '8903121126',
            'kid': 'k123'
        }
        createNewUser(user, r)
        user = {
            'name': 'kandhan',
            'username': 'kk1',
            'password': '123',
            'email': 'k@k1.com',
            'mobile': '8901121126',
            'kid': 'k124'
        }
        createNewUser(user, r)
        user = {
            'name': 'kandhan',
            'username': 'kk2',
            'password': '123',
            'email': 'k@k2.com',
            'mobile': '8902121126',
            'kid': 'k122'
        }
        createNewUser(user, r)
        userId = login(username=user['username'], password=user['password'], r=r)
        print(rank(r=r, userId=userId,top=10000))
    def testNumberOfUsers(self):
        user = {
            'name': 'kandhan',
            'username': 'kk2',
            'password': '123',
            'email': 'k@k2.com',
            'mobile': '8902121126',
            'kid': 'k122'
        }
        createNewUser(user, r)
        userId = login(username=user['username'], password=user['password'], r=r)
        self.assertEqual(getTotalNumberOfUsers(r=r),1)
        creditUserByAdmin('kk2',cash=1500,r=r)
        print(getUser(userId=userId,r=r))
