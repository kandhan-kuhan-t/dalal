import sendgrid
from sendgrid.helpers.mail import *
from gameConfig import main as Game

sg = sendgrid.SendGridAPIClient(apikey='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')
from_email = Email("dalalbull@kurukshetra.org.in")

def sendConfirmationLink(mailId, confirmationId):
    try:
        to_email = Email(mailId)
        subject = 'Confirm to play Dalal Bull!'
        message = "<h4>Click <a href='{}confirm?_cId={}'>here</a> to confirm <br/> <br/> DalalBull <br/> Kurukshetra'17 Team</h4>".format(
            Game.getMailAddress(), str(confirmationId))
        print(message)
        content = Content("text/html", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)
        return True
    except Exception:
        print('Error in sending mail')
        return False

def sendForgotPasswordLink(mailId, forgotPasswordLink):
    try:
        to_email = Email(mailId)
        subject = 'Reset Password Link!'
        message = "<h4><code>Click <a href='{}changePassword?_fGI={}'>here</a> to change your password</code> <br/> <br/> DalalBull <br/> Kurukshetra'17 Team</h4>".format(
            Game.getMailAddress(), str(forgotPasswordLink))
        content = Content("text/html", message)
        mail = Mail(from_email, subject, to_email, content)
        response = sg.client.mail.send.post(request_body=mail.get())
        print(response.status_code)
        print(response.body)
        print(response.headers)
        return True
    except Exception:
        print('Error in sending mail')
        return False