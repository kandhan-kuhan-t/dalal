import random
import redis
import pickle
import time

'''

1->Very Bad
2->Bad
3-> Experimental
4->Good
5->Very Good

'''
newslist = []
def redis_connection():
    r = redis.StrictRedis(host="localhost", port=6379, db=2)
    return r

r = redis_connection()

def getOldNews(r):
    if r.get('news') == None:
        return []
    return pickle.loads(r.get('news'))

def generateRexxonNews(newsvalue):
    rexxon = redis.StrictRedis(host="localhost", port=6380, db=1)
    returnedvalue = {}
    returnedvalue['Company'] = "Rexxon"
    rexxonlist = getOldNews(rexxon)

    countries = ["Canada", "Russia", "USA", "India", "Saudi Arabia", "Nigeria"]
    regions = ["Africa", "Siberia", "Arctic", "Arabian Sea", "Alaska"]
    returnedvalue['Value'] = newsvalue
    if newsvalue == 1:
        country = random.choice(countries)
        if country == "Canada":
            region = "Arctic"
        elif country == "India":
            region = "Arabian Sea"
        elif country == "Saudi Arabia":
            region = "Gulf"
        elif country == "USA":
            region = "Alaska"
        else:
            region = "African Sahara"
        returnedvalue["News"] = country + " releases a sanction on Rexxon to stop drilling oil in " + region + " region."

    if newsvalue == 2:
        returnedvalue["News"] = "Rexxon's Workers Union calls for a strike due to unfair wages"
    if newsvalue == 3:
        region = random.choice(regions)
        returnedvalue["News"] = "Rexxon acquires permission to search for oil in  " + region + " region."

    if newsvalue == 4:
        returnedvalue["News"] = "Rexxon posts over 10 million in profits in the last quarter."
    if newsvalue == 5:
        region = random.choice(regions)
        returnedvalue["News"] = "Rexxon's license to drill in the " + region + " region was approved."

    if rexxonlist:
        if(rexxonlist[0]['Value'] == 3):
            if newsvalue == 2:
                returnedvalue["News"] = "No Oil was found in the region and Rexxon's efforts went futile."
            else:
                returnedvalue["News"] = "Oil was found in the region and Rexxon got permission to set up drilling equipments in the " \
                          "region."
    if rexxonlist:
        if rexxonlist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateRexxonNews()
    returnedvalue["CreatedAt"] = time.time()

    rexxonlist.insert(0, returnedvalue.copy())
    rexxon.set('news', pickle.dumps(rexxonlist))
    return returnedvalue

def generateAllRexxonNews():
    rexxon = redis.StrictRedis(host="localhost", port=6380, db=1)
    rexxonNews = rexxon.get('news')
    if rexxonNews != None:
        return pickle.loads(rexxonNews)
    else:
        return []

def generateMappleNews(newsvalue):
    mapple = redis.StrictRedis(host="localhost", port=6380, db=2)

    products = ["myPhone", "myPad", "myWatche", "PacBook Pro", "PacBook Air"]
    regions = ["China", "India", "Japan", "Korea"]
    returnedvalue = {}
    mapplelist = getOldNews(mapple)

    returnedvalue["Company"] = "Mapple"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 1:
        region = random.choice(regions)
        returnedvalue["News"] = "Mapple Factory workers in " + region + " are on strike, causing halt on productions " \
                                                                        "for the last 2 weeks"
    if newsvalue == 2:
        datasets = ["1 million", "500 thousand", "2 billion"]
        dataset = random.choice(datasets)
        returnedvalue["News"] = "Hackers hack Mapple's myCloud and steal more than " + dataset + " user's data"
    if newsvalue == 3:
        product = random.choice(products)
        region = random.choice(regions)
        returnedvalue["News"] = "Mapple's new range of " + product + "s are now available in the " + region + " region"
    if newsvalue == 4:
        returnedvalue["News"] = "Mapple's profit rises more than 10% in the last month"
    if newsvalue == 5:
        region = random.choice(regions)
        returnedvalue["News"] = "Mapple acquires permission to set up new plant in the " + region + "region"

    if mapplelist:
        if mapplelist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Mapple's product was a huge failure in multiple regions."
            else:
                returnedvalue["News"] = "Mapple's product was a huge hit, selling more than 1 million units with 1 week of its launch"
    if mapplelist:
        if mapplelist[0]["News"] == returnedvalue["News"]:
            returnedvalue=generateMappleNews()
    returnedvalue["CreatedAt"] = time.time()

    mapplelist.insert(0, returnedvalue.copy())
    mapple.set('news', pickle.dumps(mapplelist))
    return returnedvalue

def generateAllMappleNews():
    mapple = redis.StrictRedis(host="localhost", port=6380, db=2)
    mappleNews = mapple.get('news')
    if mappleNews != None:
        return pickle.loads(mappleNews)
    else:
        return []

def generateGaggleNews(newsvalue):
    gaggle = redis.StrictRedis(host="localhost", port=6380, db=3)
    regions = ["Iraq", "Cayman Islands", "Switzerland", "Portugal"]
    countries = ["USA", "India", "Singapore", "Australia", "Germany"]
    products = ["Phone", "OS", "Self-Driving Car", "Video Calling Service"]
    investors = ["Sequioa Capital", "Y-Combinator", "Vijaykanth Ventures", "Golden Seeds LLC"]
    investments = ["1 million", "500 million", "1 billion"]
    returnedvalue = {}
    gagglelist = getOldNews(gaggle)
    #print(gagglelist)

    returnedvalue["Company"] = "Gaggle"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 1:
        country = random.choice(countries)
        returnedvalue["News"] = "Gaggle servers in " + country + " are down for the past 2 weeks, leading to a loss in " \
                                                                 "more than a million in revenue"
    if newsvalue == 2:
        datasets = ["1 million", "500 thousand", "2 billion"]
        dataset = random.choice(datasets)
        returnedvalue["News"] = "Hackers hack Gaggle's Mail Platform and steal more than " + dataset + " user's data"
    if newsvalue == 3:
        region = random.choice(regions)
        product = random.choice(products)
        returnedvalue["News"] = "Gaggle releases a new line of " + product + " in the " + region + " Region "

    if newsvalue == 4:
        region = random.choice(regions)
        returnedvalue["News"] = "Gaggle acquires license to set up Data Center in " + region + " Region"
    if newsvalue == 5:
        investment = random.choice(investments)
        investor = random.choice(investors)
        returnedvalue["News"] = "Gaggle raises " + investment + " dollars from " + investor

    if gagglelist:
        if gagglelist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Gaggle's product was a huge failure in multiple regions."
            else:
                returnedvalue["News"] = "Gaggle's product was a huge hit, thus increasing profits of the company by two fold"
    if gagglelist:
        if gagglelist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateGaggleNews()

    returnedvalue["CreatedAt"] = time.time()

    gagglelist.insert(0, returnedvalue.copy())
    gaggle.set('news', pickle.dumps(gagglelist))
    return returnedvalue

def generateAllGaggleNews():
    gaggle = redis.StrictRedis(host="localhost", port=6380, db=3)
    gaggleNews = gaggle.get('news')
    if gaggleNews != None:
        return pickle.loads(gaggleNews)
    else:
        return []

def generateEmptyhadNews(newsvalue):
    emptyhad = redis.StrictRedis(host="localhost", port=6380, db=4)
    countries = ["Singapore", "India", "Malaysia", "UK", "USA", "Australia", "Poland"]
    regions = ["Atlantic", "Asia-Pacific", "Gulf", "Africa", "Russian", "Arctic"]
    terrorists =["ISIS", "LeT", "Russian Bratva", "JeM", "Taliban"]
    investments = ["1 billion", "500 million", "10 billion"]
    returnedvalue = {}

    emptyhadlist = getOldNews(emptyhad)

    returnedvalue["Company"] = "Emptyhad"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 1:
        terrorist = random.choice(terrorists)
        region = random.choice(regions)
        returnedvalue["News"] = "Emptyhad's Aircraft shot down by " + terrorist + " over " + region + " airspace."

    if newsvalue == 2:
        returnedvalue["News"] = "Emptyhad voted as the Worst Aircraft in terms of User Experience in recent survey."

    if newsvalue == 3:
        returnedvalue["News"] = "Emptyhad Airways experiments with a new range of Engines for it's aircraft."

    if newsvalue == 4:
        country = random.choice(countries)
        returnedvalue["News"] = "Emptyhad Airways expands it's fleet in " + country + " thus increasing operational profits"
    if newsvalue == 5:
        country = random.choice(countries)
        investment = random.choice(investments)
        returnedvalue["News"] = "Emptyhad receives " + investment + " dollars as defence research funding " \
                                                                    "from " + country + " government"

    if emptyhadlist:
        if emptyhadlist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Emptyhad's new engine failed to pass safety checks, hence leading to a huge loss in revenue"
            else:
                returnedvalue["News"] = "Emptyhad's new engine was a huge success, leading to decrease in travel time " \
                                    "by 5%."
    if emptyhadlist:
        if emptyhadlist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateEmptyhadNews()

    returnedvalue["CreatedAt"] = time.time()

    emptyhadlist.insert(0, returnedvalue.copy())
    emptyhad.set('news', pickle.dumps(emptyhadlist))
    return returnedvalue

def generateAllEmptyhadNews():
    emptyhad = redis.StrictRedis(host="localhost", port=6380, db=4)
    emptyhadNews = emptyhad.get('news')
    if emptyhadNews != None:
        return pickle.loads(emptyhadNews)
    else:
        return []

def generateBaburNews(newsvalue):
    babur = redis.StrictRedis(host="localhost", port=6380, db=5)
    countries = ["Singapore", "India", "Malaysia", "UK", "USA", "Australia", "Poland"]
    regions = ["Atlantic", "Asia-Pacific", "Gulf", "Africa", "Russian", "Arctic"]
    investments = ["1 billion", "500 million", "10 billion"]
    products = ["Shampoo", "Toothpaste", "Soap", "Dishwasher", "Detergent"]
    newregions = ["Iraq", "Cayman Islands", "Switzerland", "Portugal"]
    returnedvalue = {}

    baburlist = getOldNews(babur)

    returnedvalue["Company"] = "Babur"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 1:
        country = random.choice(countries)
        returnedvalue["News"] = "Employees of Babur in " + country + " have gone on a strike due to unpaid wages."

    if newsvalue == 2:
        region = random.choice(regions)
        returnedvalue["News"] = "Babur's voted as the worst company in terms of user satisfaction in the " + region +" region"

    if newsvalue == 3:
        product = random.choice(products)
        returnedvalue["News"] = "Babur releases a new range of " + product

    if newsvalue == 4:
        region = random.choice(regions)
        returnedvalue["News"] = "Babur expands it's production in " + region + " thus increasing operational profits"
    if newsvalue == 5:
        nregion = random.choice(newregions)
        investment = random.choice(investments)
        returnedvalue["News"] = "Babur receives " + investment + " dollars as investment to open a new factory in " + nregion

    if baburlist:
        if baburlist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Babur's product failed to capture the market."
            else:
                returnedvalue["News"] = "Babur's product successfully managed to topple the market monopoly, " \
                                    "establishing itself as a reliable product "
    if baburlist:
        if baburlist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateBaburNews()

    returnedvalue["CreatedAt"] = time.time()

    baburlist.insert(0, returnedvalue.copy())
    babur.set('news', pickle.dumps(baburlist))
    return returnedvalue

def generateAllBaburNews():
    babur = redis.StrictRedis(host="localhost", port=6380, db=5)
    baburlistall = babur.get('news')
    if baburlistall != None:
        return pickle.loads(baburlistall)
    else:
        ret = []
        return ret

def generateSmokacolaNews(newsvalue):
    smokacola = redis.StrictRedis(host="localhost", port=6380, db=6)
    countries = ["Germany", "India", "Switzerland", "UK", "USA", "Australia", "Sweden"]
    regions = ["Atlantic", "Asia-Pacific", "Gulf", "Africa", "Russian", "Arctic", "MENAR"]
    investments = ["1 billion", "500 million", "10 billion"]
    products = ["Cornflakes", "Cold drinks", "Chocolates", "IceCream", "Biscuits"]
    newregions = ["Brazil", "Argentina", "Philippines", "Thailand", "Somalia"]
    returnedvalue = {}

    smokacolalist = getOldNews(smokacola)

    returnedvalue["Company"] = "SmokaCola"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 2:
        country = random.choice(countries)
        returnedvalue["News"] = "Employees of SmokaCola in " + country + " have" \
    " gone on a strike due to unhealthy working conditions. No production has happened for the past 2 weeks"

    if newsvalue == 1:
        region = random.choice(regions)
        returnedvalue["News"] = "SmokaCola's plant in " + region + " shut due to illegal disposal of waste into the rivers"

    if newsvalue == 3:
        product = random.choice(products)
        returnedvalue["News"] = "SmokaCola releases a new range of " + product

    if newsvalue == 4:
        region = random.choice(regions)
        returnedvalue["News"] = "SmokaCola expands it's production in " + region + ", thus increasing its quarterly profits"
    if newsvalue == 5:
        nregion = random.choice(newregions)
        investment = random.choice(investments)
        returnedvalue["News"] = "Smokacola receives " + investment + " dollars as investment to open a new factory in " + nregion

    if smokacolalist:
        if smokacolalist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Smokacola's product failed to capture the market."
            else:
                returnedvalue["News"] = "Smokacola's product successfully managed to topple the market monopoly, " \
                                    "establishing itself as a reliable product "

    if smokacolalist:
        if smokacolalist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateSmokacolaNews()

    returnedvalue["CreatedAt"] = time.time()

    smokacolalist.insert(0, returnedvalue.copy())
    smokacola.set('news', pickle.dumps(smokacolalist))
    return returnedvalue

def generateAllSmokacolaNews():
    smokacola = redis.StrictRedis(host="localhost", port=6380, db=6)
    smokacolalistall = smokacola.get('news')
    if smokacolalistall != None:
        return pickle.loads(smokacolalistall)
    else:
        ret = []
        return ret


def generateLoyalenfieldNews(newsvalue):
    loyalenfield = redis.StrictRedis(host="localhost", port=6380, db=7)
    countries = ["Pakistan", "India", "Iraq", "Afghanistan", "Nepal", "Czech Republic"]
    regions = [ "Asia-Pacific", "Gulf", "Africa", "Russian", "European" ]
    investments = ["1 billion", "500 million", "10 billion"]
    newregions = ["Brazil", "Argentina", "USA", "UK", "Syria"]

    returnedvalue = {}

    loyalenfieldlist = getOldNews(loyalenfield)

    returnedvalue["Company"] = "LoyalEnfield"

    returnedvalue["Value"] = newsvalue

    if newsvalue == 1:
        region = random.choice(regions)
        returnedvalue["News"] = "LoyalEnfield's bikes were banned in " + region + " due to high emission of pollutants"

    if newsvalue == 2:
        country = random.choice(countries)
        returnedvalue["News"] = "Employees of LoyalEnfield in " + country + " have gone on a strike due to unpaid wages."

    if newsvalue == 3:
        returnedvalue["News"] = "LoyalEnfield experiments with a new fuel system for its bikes."

    if newsvalue == 4:
        country = random.choice(countries)
        returnedvalue["News"] = "LoyalEnfield posts highest profit in " + country + " after a successful month, " \
                                                 "which saw sales shoot up by 25%, their highest in recent years"
    if newsvalue == 5:
        nregion = random.choice(newregions)
        investment = random.choice(investments)
        returnedvalue["News"] = "LoyalEnfield receives " + investment + " dollars as defence research funding " \
                                                                    "from " + nregion + " government"

    if loyalenfieldlist:
        if loyalenfieldlist[0]["Value"] == 3:
            if newsvalue == 2:
                returnedvalue["News"] = "Loyalenfield's fuel system, though appears attractive, lead to extremely low mileage"
            else:
                returnedvalue["News"] = "Loyalenfield's fuel system lead to a huge advancement in the field of automobiles."

    if loyalenfieldlist:
        if loyalenfieldlist[0]["News"] == returnedvalue["News"]:
            returnedvalue = generateLoyalenfieldNews()

    returnedvalue["CreatedAt"] = time.time()

    loyalenfieldlist.insert(0, returnedvalue.copy())
    loyalenfield.set('news', pickle.dumps(loyalenfieldlist))
    return returnedvalue

def generateAllLoyalenfieldNews():
    loyalenfield = redis.StrictRedis(host="localhost", port=6380, db=7)
    loyalenfieldlistall = loyalenfield.get('news')
    if loyalenfieldlistall != None:
        return pickle.loads(loyalenfieldlistall)
    else:
        ret = []
        return ret

def generate_news(argslist):
    newslist = getOldNews(r)
    #print('old {}'.format(newslist))

    company_name = argslist['Company']
    newsvalue = argslist['Value']
    newsvalue = int(newsvalue)
    returnedvalue = {}

    if company_name == "Rexxon":
        returnedvalue = generateRexxonNews(newsvalue)
    elif company_name == "Mapple":
        returnedvalue = generateMappleNews(newsvalue)
    elif company_name == "Gaggle":
        returnedvalue = generateGaggleNews(newsvalue)
    elif company_name == "Emptyhad":
        returnedvalue = generateEmptyhadNews(newsvalue)
    elif company_name == "Babur":
        returnedvalue = generateBaburNews(newsvalue)
    elif company_name == "Smokacola":
        returnedvalue = generateSmokacolaNews(newsvalue)
    else:
        returnedvalue = generateLoyalenfieldNews(newsvalue)

    newslist.insert(0,returnedvalue.copy())
    r.set('news', pickle.dumps(newslist))
    return returnedvalue

def getAllNews():
    newslist = getOldNews(r)
    #print(newslist)
    if newslist:
        newnewslist = pickle.loads(r.get('news'))
    else:
        newnewslist = []
    #print(newnewslist)
    return newnewslist

def testcase1():
    value = generateBaburNews()
    #print(value)

def testcase2():
    value = generateLoyalenfieldNews()
    #print(value)

def testcase3():
    allnews =  getAllNews()
    #print(allnews)

def testcase4():
    generateRexxonNews(1)
    generateRexxonNews(2)
    generateRexxonNews(4)
    generateRexxonNews(3)
    generateRexxonNews(4)
    generateRexxonNews(3)
    generateRexxonNews(2)
    generateAllRexxonNews()

def testcase5():
    '''generateGaggleNews(1)
    generateGaggleNews(2)
    generateGaggleNews(4)
    generateGaggleNews(3)
    generateGaggleNews(2)
    generateGaggleNews(3)
    generateGaggleNews(4)'''
    generateAllGaggleNews()

def clearAllCompanyNews():
    rexxon = redis.StrictRedis(host="localhost", port=6380, db=1)
    mapple = redis.StrictRedis(host="localhost", port=6380, db=2)
    gaggle = redis.StrictRedis(host="localhost", port=6380, db=3)
    emptyhad = redis.StrictRedis(host="localhost", port=6380, db=4)
    babur = redis.StrictRedis(host="localhost", port=6380, db=5)
    smokacola = redis.StrictRedis(host="localhost", port=6380, db=6)
    loyalenfield = redis.StrictRedis(host="localhost", port=6380, db=7)
    allnews = redis.StrictRedis(host="localhost", port=6379, db=2)
    mapple.flushdb()
    rexxon.flushdb()
    gaggle.flushdb()
    emptyhad.flushdb()
    babur.flushdb()
    smokacola.flushdb()
    loyalenfield.flushdb()
    allnews.flushdb()
if __name__ == '__main__':
    #r.flushdb()
    clearAllCompanyNews()
    #testcase5()
    #generate_news()
    #getAllNews()
    #testcase1()
    #testcase2()
    #testcase3()
    #print('pickled loads list')
    #print(pickle.loads(r.get('news')))
