LOCAL = True


def defaultUserCash():
   return 10000

def companies():
    li = ['babur', 'emptyhad', 'gaggle', 'loyalenfield', 'mapple', 'rexxon', 'smokacola']
    return li

def getTickerPort():
    return 8

def orderStatus(number):
    obj = {
        -7:'Your order has been Executed',
         2:'Your order has been Added',
         3:'Your order has been Cancelled due to Insufficient Cash',
        -8:'Your order has been Cancelled due to Insufficient Stocks',
        -10:'Your order has been Partially Executed and added to Order Book'
    }
    return obj[number]

def getTickerPrice(company):
    tickerPrice={
        'gaggle':95,
        'emptyhad':83,
        'smokacola':67,
        'babur':52,
        'loyalenfield':156,
        'mapple':75,
        'rexxon':336
    }
    return tickerPrice[company]

def getMaxUnitFluctuationPercent():
    return 2

def getMaxFluctuationPercent():
    return 30

def getMinimumAmountForFluctuation():
    return (defaultUserCash()/100)*7

def getAppAddress():
    return 'http://127.0.0.1:5003'

def getMailAddress():
    return 'http://139.59.12.247/'
