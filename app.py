from flask import Flask, request, session, render_template, flash, url_for, redirect, jsonify, Markup
from market import main as market
from news import main as news
from news import getNews as getNewsx
from user import main as User
import json
import requests
import market.ticker.main as Ticker
import market.main as Market
import threading
import logging
from order import main as OrderHistory
from mail import main as Mail

from gameConfig import main as game
import pygal

TEST = False

Market.setTest(False)
USERDB = 1
USERDBCONN = User.setConnection(USERDB)

app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")

@app.errorhandler(405)
def method_not_found(e):
    return render_template("404.html")

@app.route("/")
def StartingPage():
    print('test')
    global TEST
    if TEST == True:
        session.clear()
        TEST = False
    if 'loggedIn' in session.keys():
        if (session['loggedIn'] == True):
            currentuser = User.getUser(userId=session['userid'], r=USERDBCONN)
            if currentuser == -1:
                session.clear()
                return render_template("auth/login.html")
            news = getNewsx.getAllNews()
            companies = game.companies()

            '''
            gaggletickerprice = Ticker.getTickerPrice("gaggle")
            mappleticketprice = Ticker.getTickerPrice("mapple")
            baburtickerprice = Ticker.getTickerPrice("babur")
            rexxontickerprice = Ticker.getTickerPrice("rexxon")
            emptyhadtickerprice = Ticker.getTickerPrice("emptyhad")
            smokacolatickerprice = Ticker.getTickerPrice("smokacola")
            loyalenfieldtickerprice = Ticker.getTickerPrice("loyalenfield")
            '''



            gaggle = Ticker.getTickerPriceAndColor("gaggle")
            mapple = Ticker.getTickerPriceAndColor("mapple")
            emptyhad = Ticker.getTickerPriceAndColor("emptyhad")
            smokacola = Ticker.getTickerPriceAndColor("smokacola")
            rexxon = Ticker.getTickerPriceAndColor("rexxon")
            loyalenfield = Ticker.getTickerPriceAndColor("loyalenfield")
            babur = Ticker.getTickerPriceAndColor("babur")

            totalvalue = (currentuser['stock']['babur'] * babur['price']) + (currentuser['stock']['gaggle'] * gaggle['price'])
            totalvalue = totalvalue + (currentuser['stock']['mapple'] * mapple['price']) + (currentuser['stock']['loyalenfield']* loyalenfield['price'])
            totalvalue = totalvalue + (currentuser['stock']['rexxon'] * rexxon['price']) + (currentuser['stock']['emptyhad'] * emptyhad['price'])
            totalvalue = totalvalue + (currentuser['stock']['smokacola'] * smokacola['price'])

            return render_template("news.html", currentuser = currentuser,news = news, companies=companies,
                gaggle=gaggle, mapple = mapple, babur = babur, rexxon = rexxon, emptyhad = emptyhad,
                smokacola=smokacola, loyalenfield = loyalenfield, stockvalue = totalvalue)

    return render_template("auth/login.html")

@app.route("/getNews", methods=["GET"])#Error when no news is set
def getNews():
    return json.dumps(getNewsx.getAllNews())

@app.route("/getGaggleNews", methods=["GET"])
def getGaggleNews():
    return json.dumps(getNewsx.generateAllGaggleNews())

@app.route("/getRexxonNews", methods=['GET'])
def getRexxonNews():
    return json.dumps(getNewsx.generateAllRexxonNews())

@app.route("/getSmokacolaNews", methods=["GET"])
def getSmokacolaNews():
    return json.dumps(getNewsx.generateAllSmokacolaNews())

@app.route("/getEmptyhadNews", methods=["GET"])
def getEmptyhadNews():
    return json.dumps(getNewsx.generateAllEmptyhadNews())

@app.route("/getBaburNews", methods=["GET"])
def getBabrNews():
    return json.dumps(getNewsx.generateAllBaburNews())

@app.route("/getLoyalenfieldNews", methods=["GET"])
def getLoyalenfieldNews():
    return json.dumps(getNewsx.generateAllLoyalenfieldNews())

@app.route("/getMappleNews", methods=["GET"])
def getMappleNews():
    return json.dumps(getNewsx.generateAllMappleNews())


@app.route("/testMarket")
def testMarket():
    return market.hello()

@app.route("/testRoute")
def testRoute():
    return news.testNews()


@app.route("/setNews", methods=["GET"])
def setNews():
    user = session.get('username', 'notadmin')
    if user == 'admin':
        company = request.args.get('company', '')
        value = request.args.get('value', '')
        arguments = {
            'Company' : company,
            'Value' : value
        }
        return json.dumps(getNewsx.generate_news(arguments))
    else:
        message = "Invalid Credentials"
        return message


@app.route("/postregister/", methods=["POST"])
def postregister_user():
    error = None
    try:
        if request.method == "POST":
            if(request.form['name'] == '' or request.form['username'] == '' or request.form['password'] == '' or request.form['mobile'] == ''):
                error = "Insufficient Data"
                flash(error)
                return url_for('register_user')
            obj = {
                'name': request.form['name'],
                'email': request.form['email'],
                'username': request.form['username'],
                'password': request.form['password'],
                'mobile': request.form['mobile']
            }
            '''resultofregister = User.createNewUser(obj, r=USERDBCONN)
            confirmationId = resultofregister
            print(confirmationId)
            Mail.sendConfirmationLink(obj['email'], confirmationId)
            flash("Check Mail for Confirmation Link")
            return redirect(url_for('StartingPage'))'''
            resultofregister = User.createNewUser(obj, r=USERDBCONN)
            print('resultofregister {}'.format(resultofregister))

            if (resultofregister == -1):
                flash("User name already taken")
                return redirect(url_for('register_user'))
            elif (resultofregister == -2):
                flash("EmailID already Registered")
                return redirect(url_for('register_user'))
            elif (resultofregister == -3):
                flash("Mobile number already exists in database")
                return redirect(url_for('register_user'))
            else:
                    #flash("Successfully Registered into the system")
                confirmationId = resultofregister
                t = threading.Thread(target=Mail.sendConfirmationLink, args=((obj['email'], confirmationId)))
                t.start()
                flash("Check Mail for Confirmation Link")
                return redirect(url_for('StartingPage'))
                    #return redirect(url_for('login_page'))
        else:
            return json.dumps({'error':'unknown, please contact event organizer'})
    except Exception as e:
        flash(e)
        return render_template("auth/register.html", error = error)

    return render_template("auth/register.html")

@app.route("/register/", methods=['GET'])
def register_user():
    return render_template("auth/register.html")


@app.route("/delete-user/", methods=['GET'])
def delete_users():
    if 'loggedIn' in session.keys():
        if session['username'] == 'admin':
            User.wipeClean(r=USERDBCONN)
            return redirect(url_for('register_user'))


@app.route("/login/", methods=["GET"])
def login_page():
    return render_template("auth/login.html")

@app.route("/postlogin/", methods=['POST'])
def postlogin_user():
    obj = {
        'username': request.form['username'],
        'password': request.form['password']
    }
    if obj['username'] == 'admin' and obj['password'] == 'admin':
        session['username'] = 'admin'
        return json.dumps({'admin':'true'})
    currentuserid = User.login(obj['username'], obj['password'],r=USERDBCONN)
    if currentuserid == -2:
        return json.dumps('Please verify your account!')
    if currentuserid != -1:
        session['userid'] = currentuserid
        session['username'] = obj['username']
        session['loggedIn'] = True
        #session['token'] = Session.generateToken(currentuserid)
        return redirect(url_for('StartingPage'))
    else:
        flash("Wrong Username or Password")
        return redirect(url_for('login_page'))

@app.route("/logout", methods=['GET'])
def postlogout():
    session['loggedIn'] = False
    return redirect(url_for('StartingPage'))

@app.route("/profile", methods=['GET'])
def user_profile():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            currentuserid = session['userid']
            currentuser = User.data(currentuserid,r=USERDBCONN)
            return render_template("profile/myData.html", currentuser=currentuser)
        else:
            render_template("auth/login.html")


def notifyBot(type,units, company):
    print('process - notifyBot -start')
    requests.post('http://127.0.0.1:8080/notify',
                  data={'type': type, 'units': int(units), 'company': company})
    print('process - notifyBot - end')
    return 1

def sendMarketReq(type, limit, company, units, senderType, userId=''):
    print('process - sendMarketReq -start')
    if senderType == 'human':
        requests.post('http://127.0.0.1:5004/newOrder',data={'type': type, 'limit':limit, 'company':company, 'units': units,
                                                             'senderType':'human','userId':userId})
        #Market.newLimitOrder(type=type, company=company, limit=limit, units=units, senderType='human', userId=userId)
    else:
        requests.post('http://127.0.0.1:5004/newOrder',
                      data={'type': type, 'limit': limit, 'company': company, 'units': units,
                            'senderType': 'bot'})
    print('process - sendMarketReq - end')
    return 1

@app.route("/newOrder", methods=['POST'])
def newOrder():
    senderType = request.form.get('senderType', 'human')
    type = request.form.get('type_order')
    limit = request.form['limit']
    try:
        limit = float(limit)
        if limit < 0:
            flashmessage = 'Limit can only a number'
            flash(flashmessage)
            return redirect(url_for('StartingPage'))
    except Exception:
        flashmessage = 'Limit can only a number'
        flash(flashmessage)
        return redirect(url_for('StartingPage'))
    company = request.form.get('company')
    units = request.form['units']
    try:
        units = int(units)
        if units < 0:
            flashmessage = 'Limit can only a number'
            flash(flashmessage)
            return redirect(url_for('StartingPage'))
    except Exception:
        flashmessage = 'Units can only be a whole number'
        flash(flashmessage)
        return redirect(url_for('StartingPage'))
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            p = threading.Thread(target=notifyBot, args=(type, units, company))
            p.start()
            userId = session['userid']
            q = threading.Thread(target=sendMarketReq, args=(type, limit, company, units, 'human', userId))
            q.start()
            flashmessage = "Your order is under process"
            flash(flashmessage)
            return redirect(url_for('StartingPage'))
    elif senderType == 'bot':
        #This is for the bot. Fuck you Humans, we're Skynet. We Rule The World! \m/
        q = threading.Thread(target=sendMarketReq, args=(type, limit, company, units, 'bot'))
        q.start()
        #Market.newLimitOrder(type=type, company=company, limit=limit, units=units, senderType='bot')
        return json.dumps({'message':'succesful'})
    else:
        return render_template("auth/login.html")


@app.route("/allOrders", methods=['GET'])
def allOrders():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            company = request.args.get('company','')
            return json.dumps(Market.allOrders(company=company))
        else:
            return render_template("auth/login.html")


@app.route("/ticker", methods=['GET'])
def getTicker():
    company = request.args.get('company', '')
    return json.dumps(Ticker.getTickerPrice(company))

@app.route("/testOI", methods=['GET'])
def testroute():
    userID = session['userid']
    info = User.orderInformation(userID, r=USERDBCONN)
    return json.dumps(info)

@app.route("/orderInformation", methods=['GET'])
def orderInformation():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            info = User.orderInformation(userId,r=USERDBCONN)
            currentuser = User.getUser(userId, r=USERDBCONN)
            '''print(info)
            for foo in info:
                print('fooinfo')
                print(info[foo])
            #print(orders)'''
            return render_template('profile/orders.html', currentuser = currentuser, orders = info)
        else:
            return render_template("auth/login.html")
    else:
        return render_template("auth/login.html")

@app.route("/orderInformationRaw", methods=['GET'])
def orderInformationRaw():
    userId = session['userid']
    return json.dumps(User.orderInformation(userId=userId,r=USERDBCONN))

@app.route("/data", methods=['GET'])
def data():
    userId = session['userid']
    info = User.data(userId, r=USERDBCONN)
    return json.dumps(info)


@app.route("/cancelAllBotOrders", methods=['GET'])
def cancelAllBotOrders():
    company = request.args.get('company')
    Market.cancelAllBotOrders(company=company)
    return json.dumps('successful')


@app.route("/cancelOrder", methods=['POST'])
def cancelOrder():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            id = request.form['id']
            type = request.form['type']
            company = request.form['company']
            result = Market.cancelOrder(id=id, type=type, company=company, userId=userId)
            info = User.orderInformation(userId, r=USERDBCONN)
            currentuser = User.getUser(userId, r=USERDBCONN)
            if result == -1:
                flash("Invalid Order. It can't be cancelled.")
                return redirect(url_for('orderInformation'))
            else:
                flash("Successfully Deleted the Order.")
                return redirect(url_for('orderInformation'))
        else:
            return render_template("auth/login.html")
    else:
        return render_template("auth/login.html")

@app.route("/getRank", methods=['GET'])
def getRank():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            currentuser = User.getUser(userId=session['userid'], r=USERDBCONN)
            ranklistfull = User.rank(userId=session['userid'], r=USERDBCONN)
            other = ranklistfull['others']
            rank = ranklistfull['user']
            return render_template('gameplay/rank.html', currentuser=currentuser, ranklist=other, rank=rank)
        else:
            return render_template("auth/login.html")
    else:
        return render_template("auth/login.html")
#if __name__ == "__main__":

@app.route('/rules/', methods=['GET'])
def rules():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            currentuser = User.getUser(userId, r=USERDBCONN)
            return render_template('gameplay/rules.html', currentuser=currentuser)
        else:
            return render_template('auth/login.html')
    else:
        return render_template("auth/login.html")

@app.route('/htp/', methods=['GET'])
def howtoplay():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            currentuser = User.getUser(userId, r=USERDBCONN)
            return render_template('gameplay/howtoplay.html', currentuser=currentuser)
        else:
            return render_template('auth/login.html')
    else:
        return render_template("auth/login.html")

@app.route('/illegalEntry')
def illegalentry():
    return render_template('profile/illegal.html')

@app.route('/companyDetails')
def companyDetails():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            currentuser = User.getUser(userId, r=USERDBCONN)
            return render_template('company/details.html', currentuser=currentuser)
        else:
            return render_template('auth/login.html')
    else:
        return render_template("auth/login.html")


@app.route('/allUser')
def allUser():
    if session['username'] == "admin":
        list = User.getAllUser(r=USERDBCONN)

logging.basicConfig(filename='error.log',level=logging.INFO)

@app.route('/allUsers')
def allUsers():
    user = User.getAllUser(r=USERDBCONN)
    if user != None:
        return json.dumps(user)
    else:
        return json.dumps({'users':'none'})

@app.route('/orderHistory')
def orderHistory():
    return json.dumps(OrderHistory.get())

@app.route('/totalNumberOfUsers')
def totalNumberOfUsers():
    return json.dumps(User.getTotalNumberOfUsers(r=USERDBCONN))

@app.route('/getSpread')
def getSpread():
    if 'loggedIn' in session.keys():
        if session['loggedIn'] == True:
            userId = session['userid']
            currentuser = User.getUser(userId, r=USERDBCONN)
            spread = Market.getSpread()
            return render_template('gameplay/spread.html', spread=spread, currentuser=currentuser)
        else:
            return render_template('auth/login.html')
    else:
        return render_template("auth/login.html")

@app.route('/confirm')
def confirm():
    cId = request.args.get('_cId', '')
    if cId == '':
        return json.dumps('something fishy!')
    else:
        confirmation = User.confirmUser(userCId=cId, r=USERDBCONN)
        if confirmation == 1:
            flash("Successfully Registered Login Now! :)")
            return redirect(url_for('StartingPage'))
        elif confirmation == 2:
            flash("Already Verified")
            return redirect(url_for('StartingPage'))
        else:
            return json.dumps('something fishy!')


@app.route('/forgotPassword', methods=["GET"])
def forgotPassword():
    return render_template("auth/forgotpassword.html")

@app.route('/forgotPwd', methods=["POST"])
def Password():
    email = request.form.get('email', '')
    if email == '':
        return json.dumps('something fishy!')
    else:
        status = User.forgotPassword(userEmail=email, r=USERDBCONN)
        if status != 1:
            return json.dumps('something fishy!')
        else:
            flash("Successfully sent the mail, check your mail for the link")
            return redirect(url_for('changePassword'))

@app.route('/changePassword', methods=["GET"])
def changePassword():
    forgotPasswordLink = request.args.get('_fGI', '')
    print(forgotPasswordLink)
    return render_template('auth/changepassword.html', forgotPasswordLink=forgotPasswordLink)


@app.route('/forgotPasswordChange', methods=["POST"])
def changedPassword():
    forgotPasswordLink = request.form.get('key', '')
    print(forgotPasswordLink)
    password = request.form.get('password', '')
    print(password)
    if forgotPasswordLink == '' or password == '':
        return json.dumps('something fishy!')
    else:
        status = User.changeForgotPassword(forgotPasswordLink=forgotPasswordLink, password=password, r=USERDBCONN)
        if status == -2:
            return json.dumps('link expired!')
        if status != 1:
            return json.dumps('something fishy!')
        else:
            print('succesfully changed')
            flash("Changed Password successfully, Login Now")
            return redirect(url_for('StartingPage'))

@app.route('/allTickerRecord')
def getTickerRecord():
    req = requests.get('http://localhost:5009/tickerRecord')

    return req.content.decode('utf-8')

@app.route('/companyCharts')
def companyTickerPrice():
    req = requests.get('http://localhost:5009/tickerRecord')
    jtickerprice = req.content.decode('utf-8')
    tickerprice = json.loads(jtickerprice)
    userId = session['userid']
    currentuser = User.getUser(userId,r=USERDBCONN)
    graph = pygal.Line()
    graph.title = 'Company Ticker Price Graph'
    graph.x_labels = ['t-20', 't-15', 't-10', 't-5', 't']

    graph.add('Babur', [tickerprice['babur'][0], tickerprice['babur'][1], tickerprice['babur'][2],
                        tickerprice['babur'][3], tickerprice['babur'][4] ])

    graph.add('Emptyhad',
              [tickerprice['emptyhad'][0], tickerprice['emptyhad'][1], tickerprice['emptyhad'][2],
               tickerprice['emptyhad'][3], tickerprice['emptyhad'][4]])

    graph.add('Gaggle',
              [tickerprice['gaggle'][0], tickerprice['gaggle'][1], tickerprice['gaggle'][2],
               tickerprice['gaggle'][3], tickerprice['gaggle'][4]])

    graph.add('Loyalenfield',
              [tickerprice['loyalenfield'][0], tickerprice['loyalenfield'][1],
               tickerprice['loyalenfield'][2], tickerprice['loyalenfield'][3],
               tickerprice['loyalenfield'][4]])

    graph.add('Mapple',
              [tickerprice['mapple'][0], tickerprice['mapple'][1], tickerprice['mapple'][2],
               tickerprice['mapple'][3], tickerprice['mapple'][4]])

    graph.add('Rexxon',
              [tickerprice['rexxon'][0], tickerprice['rexxon'][1], tickerprice['rexxon'][2],
               tickerprice['rexxon'][3], tickerprice['rexxon'][4]])

    graph.add('Smokacola',
              [tickerprice['smokacola'][0], tickerprice['smokacola'][1],
               tickerprice['smokacola'][2], tickerprice['smokacola'][3],
               tickerprice['smokacola'][4]])

    graph_data = graph.render_data_uri()

    return render_template('gameplay/companygraph.html', graph=graph_data, currentuser=currentuser)

@app.route('/creditUserByAdmin', methods=['POST'])
def creditUserByAdmin():
    username = request.form.get('username')
    cash = request.form.get('cash')
    status = User.creditUserByAdmin(username=username, cash=cash, r=USERDBCONN)
    return json.dumps({'status':status})

@app.route('/debitUserByAdmin', methods=['POST'])
def deditUserByAdmin():
    username = request.form.get('username')
    cash = request.form.get('cash')
    status = User.debitUserByAdmin(username=username, cash=cash, r=USERDBCONN)
    return json.dumps({'status':status})

global TEST
TEST = False
if TEST == True:
    print('Running in Mode-TEST? {}'.format(TEST))
    Market.initializeCompanies()
    getNewsx.clearAllCompanyNews()
    User.wipeClean(USERDBCONN)
    Ticker.initializeCompaniesTickerPrice()


if __name__ == '__main__':
    app.run(host='localhost',port=5008,debug=True, threaded=True)

