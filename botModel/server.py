from flask import Flask, request
import json
import botModel.bot as bot
import threading

app = Flask(__name__)

@app.route("/notify",methods=["POST"])
def notify():
    t = threading.Thread(target=gaggleBot.newOrderNotification,args=(request.form['type'],request.form['units']))
    t.start()
    return json.dumps({'message': 'succesful'})

if __name__ == '__main__':
    global gaggleBot
    gaggleBot = bot.Bot(company='gaggle',newsLink='getGaggleNews')
    app.run(port=8080)

