from flask import Flask, request
import json

app = Flask(__name__)

import bot as bot
#import botModel.bot as bot
import threading


@app.route("/notify",methods=["POST"])
def notify():
    if request.form['company'] == 'gaggle':
        t = threading.Thread(target=gaggleBot.newOrderNotification,args=(request.form['type'],request.form['units']))
        t.start()
    elif request.form['company'] == 'loyalenfield':
        t = threading.Thread(target=loyalBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    elif request.form['company'] == 'rexxon':
        t = threading.Thread(target=rexxonBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    elif request.form['company'] == 'emptyhad':
        t = threading.Thread(target=emptyHadBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    elif request.form['company'] == 'smokacola':
        t = threading.Thread(target=smokaColaBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    elif request.form['company'] == 'babur':
        t = threading.Thread(target=baburBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    elif request.form['company'] == 'mapple':
        t = threading.Thread(target=mappleBot.newOrderNotification, args=(request.form['type'], request.form['units']))
        t.start()
    return json.dumps({'message': 'succesful'})

'''if __name__ == '__main__':
    global gaggleBot
    global loyalBot
    global rexxonBot
    global emptyHadBot
    global smokaColaBot
    global baburBot
    global mappleBot
    gaggleBot = bot.Bot(company='gaggle',newsLink='getGaggleNews')
    loyalBot = bot.Bot(company='loyalenfield', newsLink= 'getLoyalenfieldNews')
    rexxonBot = bot.Bot(company='rexxon', newsLink='getRexxonNews')
    emptyHadBot = bot.Bot(company='emptyhad',newsLink='getEmptyhadNews')
    smokaColaBot = bot.Bot(company='smokacola', newsLink='getSmokacolaNews')
    baburBot = bot.Bot(company='babur', newsLink='getBaburNews')
    mappleBot = bot.Bot(company='mapple', newsLink='getMappleNews')
    app.run()'''
global gaggleBot
global loyalBot
global rexxonBot
global emptyHadBot
global smokaColaBot
global baburBot
global mappleBot
gaggleBot = bot.Bot(company='gaggle',newsLink='getGaggleNews')
loyalBot = bot.Bot(company='loyalenfield', newsLink= 'getLoyalenfieldNews')
rexxonBot = bot.Bot(company='rexxon', newsLink='getRexxonNews')
emptyHadBot = bot.Bot(company='emptyhad',newsLink='getEmptyhadNews')
smokaColaBot = bot.Bot(company='smokacola', newsLink='getSmokacolaNews')
baburBot = bot.Bot(company='babur', newsLink='getBaburNews')
mappleBot = bot.Bot(company='mapple', newsLink='getMappleNews')

