import requests
import sys
sys.path.append('../')
import gameConfig.main as game
import math
import unittest
import random
from decimal import *
getcontext().prec = 3

TEST = False

def percent(ticker, percentage):
    percentage = percentage + (random.choice([-1,1])*(float(Decimal(random.uniform(0.0,0.2))/1)))
    return (ticker/100) * percentage

def testGetTickerPrice():
    return TICKERPRICE

def against(type):
    if type == 'sell':
        return 'buy'
    elif type == 'buy':
        return 'sell'

class Bot(object):
    def __init__(self, company, newsLink):
        self.company = company
        self.newsLink = newsLink
        print('Bot for company-{}, newslink-{}'.format(self.company, self.newsLink))
        self.context = self.getNewsValue()
        self.contextSwitch()

    def getTickerPrice(self):
        if TEST == False:
            r = requests.get(game.getAppAddress() + '/ticker?company=' + self.company)
            ticker = r.json()
            print('bot:: ticker: {}'.format(ticker))
            return ticker
        else:
            return testGetTickerPrice()

    def switchContextIfNeeded(self, newsValue):
        print('Switch context, context-{}, newsValue - {}'.format(self.context,newsValue))
        if self.context != newsValue:
            self.context = newsValue
            self.contextSwitch()
            self.cancelOrdersUponContextSwitch()
        return 1

    def getNewsValue(self):
        if TEST == False:
            try:
                r = requests.get(game.getAppAddress() + '/' + self.newsLink)
                news = r.json()
                return news[0]['Value']
            except Exception:
                return 0
        else:
            return NEWSVALUE

    def placeOrder (self, order):
        if TEST == False:
            r = requests.post(game.getAppAddress() + '/newOrder',
                              data={
                                  'type_order': order['type_order'],
                                  'company': order['company'],
                                  'units': order['units'],
                                  'limit': order['limit'],
                                  'senderType': 'bot'
                              })
            return r.json()
        else:
            data = {
                'type_order': order['type_order'],
                'company': order['company'],
                'units': order['units'],
                'limit': order['limit'],
                'senderType': 'bot'
            }
            return data

    def newOrderNotification(self, type, units):
        units = int(units)
        newsValue = self.getNewsValue()
        self.switchContextIfNeeded(newsValue=newsValue)
        ticker = self.getTickerPrice()
        '''randomOrderProb = random.choice([2,4,1]), if odd -> send randomOrder; probability = 1/3'''
        randomOrderProb = random.choice([2,4,1])
        #if randomOrderProb %2 != 0: #Odd
            #self.sendRandomOrder()
        unitCompleteOrNot = random.choice([2, 4, 6, 8, 1])
        unitCompleteOrNot = 2
        if unitCompleteOrNot % 2 == 0:
            unitsToBePlaced = units
        else:
            unitsToBePlaced = max(
                round((units * 2) / 3) + ((random.choice([-1, +1])) * random.randrange(0, round(units), 1)),
                round(units / 2))
        if self.context == 0 or self.context == 3: #No context
            extraLimit = random.choice([-1,+1])*(percent(ticker,percentage=self.minPercentage))
            limit = ticker + extraLimit
            order = {
                'type_order': against(type),
                'company': self.company,
                'units':  unitsToBePlaced,
                'limit': limit
            }
            response = self.placeOrder(order)
            print('bot: got response {}'.format(response))
            return response
        else:
            if type == 'buy':
                '''Send a sell request'''
                ticker = self.getTickerPrice()
                order = {
                    'type_order': 'sell',
                    'company': self.company,
                    'units': unitsToBePlaced,
                    'limit': percent(ticker, self.minPercentage) + ticker
                }
                response = self.placeOrder(order)
                print('response {}'.format(response))
                return response
            else:
                '''Send a buy request'''
                ticker = self.getTickerPrice()
                order = {
                    'type_order': 'buy',
                    'company': self.company,
                    'units': unitsToBePlaced,
                    'limit': percent(ticker, self.minPercentage) + ticker
                }
                response = self.placeOrder(order)
                print('response {}'.format(response))
                return response


    def sendRandomOrder(self):
        '''Pick a type'''
        '''Pick number of units'''
        '''Select limit by context'''
        print('bot:: Random Order')
        type = random.choice(['buy', 'sell'])
        unitsToBePlaced = random.randrange(3, 30, 1)
        ticker = self.getTickerPrice()
        if self.context == 0 or self.context == 3: #No context
            extraLimit = random.choice([-1,+1])*(percent(ticker,percentage=self.minPercentage))
            limit = ticker + extraLimit
        else:
            limit = percent(ticker, self.minPercentage) + ticker
        order = {
            'type_order': type,
            'company': self.company,
            'units': unitsToBePlaced,
            'limit': limit
        }
        response = self.placeOrder(order)
        print('bot: got response {}'.format(response))
        return response


    def cancelOrdersUponContextSwitch(self):
        if TEST == False:
            r = requests.get(game.getAppAddress()+'/'+'cancelAllBotOrders?company='+self.company)
            print('bot: cancel status {}'.format(r.json))
            return 1
        else:
            print('bot: orders cancelled')
            return 1

    def contextSwitch(self):
        print('Context-Switch {}'.format(self.context))
        if self.company == 'rexxon' or self.company == 'loyalenfield':
            if self.context == 4:
                self.minPercentage = 0.4
                self.maxPercentage = 0.4
            if self.context == 5:
                self.minPercentage = 0.8
                self.maxPercentage = 0.8
            if self.context == 3:
                self.minPercentage = 0.5
                self.maxPercentage = 0.5
            if self.context == 2:
                self.minPercentage = -0.4
                self.maxPercentage = -0.4
            if self.context == 1:
                self.minPercentage = -0.8
                self.maxPercentage = -0.8
            elif self.context == 0:  # No-context
                self.minPercentage = 0.5
                self.maxPercentage = 0.5
        else:
            if self.context == 4:
                self.minPercentage = 1
                self.maxPercentage = 1
            if self.context == 5:
                self.minPercentage = 1.5
                self.maxPercentage = 1.5
            if self.context == 3:
                self.minPercentage = 0.5
                self.maxPercentage = 0.5
            if self.context == 2:
                self.minPercentage = -1
                self.maxPercentage = -1
            if self.context == 1:
                self.minPercentage = -1.5
                self.maxPercentage = -1.5
            elif self.context == 0:  # No-context
                self.minPercentage = 0.5
                self.maxPercentage = 0.5
        return


if __name__ == '__main__':
    global TEST
    global NEWSVALUE
    global TICKERPRICE
    TEST = True
    newsValue = 0
    TICKERPRICE = 12
    bot = Bot('gaggle','getGaggleNews')
    bot.newOrderNotification(type='buy',units=12)

class tests(unittest.TestCase):
    @classmethod
    def setUp(self):
        global TEST
        global NEWSVALUE
        global TICKERPRICE
        global bot
        bot = Bot('gaggle', 'getGaggleNews')
        TEST = True
        newsValue = 0
    def testSellOnContext0(self):
        global TICKERPRICE
        global NEWSVALUE
        TICKERPRICE = 12
        NEWSVALUE = 4
        print(bot.newOrderNotification(type='buy',units=12))
        TICKERPRICE = 12.4
        NEWSVALUE = 1
        print(bot.newOrderNotification(type='buy', units=12))
