from flask import Flask, request, session, render_template, flash, url_for, redirect, jsonify
from market import main as market
import market.ticker.main as Ticker
import market.main as Market
import threading
import requests
from gameConfig import main as game
import json

Market.setTest(False)


app = Flask(__name__)

app.config.update(dict(
    SECRET_KEY='development key',
    USERNAME='admin',
    PASSWORD='default'
))


@app.route("/newOrder", methods=['POST'])
def newOrder():
    print('new server')
    senderType = request.form.get('senderType', 'human')
    if senderType == 'human':
        print(Market.newLimitOrder(type=request.form['type'],company=request.form['company'],limit=request.form['limit'],
                         units=request.form['units'],senderType='human',userId=request.form['userId']))
    else:
        print(Market.newLimitOrder(type=request.form['type'], company=request.form['company'],
                                   limit=request.form['limit'],
                                   units=request.form['units'], senderType='bot'))
    return json.dumps({'message':'successful'})


