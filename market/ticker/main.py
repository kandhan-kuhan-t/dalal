from gameConfig import main as game
import redis
import pickle
import math
import unittest
import random
import time

r=redis.StrictRedis(host='localhost',port=6379,db=game.getTickerPort())

def initializeCompaniesTickerPrice():
    companies = game.companies()
    for company in companies:
        setTickerPriceInitial(company,game.getTickerPrice(company))
    return 1

def getTickerPrice(company):
    return pickle.loads(r.get(company))['price']

def getTickerPriceAndColor(company):
    return pickle.loads(r.get(company))

def setTickerPriceInitial(company, price):
    obj = {
        'price':price,
        'color': None
    }
    r.set(company, pickle.dumps(obj))
    return 1

def setTickerPrice(company, price):
    obj = {
        'price':price
    }
    prevPrice = getTickerPrice(company)
    if price == prevPrice:
        obj['color'] = None
    elif price > prevPrice:
        obj['color'] = 'green'
    else:
        obj['color'] = 'red'
    r.set(company, pickle.dumps(obj))
    return 1


def percentValue(number,x):
    return (number/100)*x

def orderWeight(executedPrice, units):
    amountSpent = executedPrice*units
    userCash = game.defaultUserCash()
    percentSpent = (amountSpent*100)/userCash
    if percentSpent >= 10:
        return 1
    else:
        return percentSpent/10

def updateTicker(executedPrice, company, units):
    initialTicker = game.getTickerPrice(company)
    currentTicker = getTickerPrice(company)

    #Invariant Number One -> TickerPrice should never go up or down by more than 130% or less than 70% from intialTickerPrice
    maxTicker = percentValue(initialTicker, game.getMaxFluctuationPercent())+initialTicker
    minTicker = initialTicker - percentValue(initialTicker, game.getMaxFluctuationPercent())
    print('ticker:: incoming exec price: {}'.format(executedPrice))
    '''executedPrice = (executedPrice*orderWeight(executedPrice,units))'''
    print('ticker:: exec price after weight fn: {}'.format(executedPrice))
    if executedPrice > maxTicker :
        print('ticker:: maxTicker')
        maxUpdate = min(maxTicker * random.uniform(1.0,1.01),percentValue(currentTicker, game.getMaxUnitFluctuationPercent()) + currentTicker)
        setTickerPrice(company, maxUpdate)
        return 1
    elif executedPrice < minTicker:
        print('ticker:: minTicker')
        maxUpdate = max(minTicker * random.uniform(0.99,1.0),currentTicker - percentValue(currentTicker, game.getMaxUnitFluctuationPercent()))
        setTickerPrice(company, maxUpdate)
        return 1

    #Invariant Number Two -> TickerPrice should never vary by more than 5% of its previousValue, update it to maxLimit(4.99%)
    maxUnitFluctuation = percentValue(currentTicker,game.getMaxUnitFluctuationPercent())

    if executedPrice >= currentTicker:
        if (executedPrice - currentTicker) > maxUnitFluctuation:
            print('ticker:: maxUnitFluc')
            maxUpdate = percentValue(currentTicker, game.getMaxUnitFluctuationPercent()) - (random.uniform(0, 0.2)) + currentTicker
            print('ticker:: {}'.format(maxUpdate))
            setTickerPrice(company, maxUpdate)
            return 1
    elif executedPrice < currentTicker:
        if (currentTicker - executedPrice) > maxUnitFluctuation:
            print('ticker::  minUnitFluc')
            maxUpdate = currentTicker - percentValue(currentTicker, game.getMaxUnitFluctuationPercent()) + (random.uniform(0, 0.2))
            print('ticker::  maxUpdate {}'.format(maxUpdate))
            setTickerPrice(company, maxUpdate)
            return 1
    print('ticker:: usual update')
    print(executedPrice)
    updatePrice=executedPrice
    setTickerPrice(company,updatePrice)


class tests(unittest.TestCase):
    def testInitialize(self):
        initializeCompaniesTickerPrice()
        for company in game.companies():
            self.assertEqual(type(getTickerPrice(company)), type(1))
            print(getTickerPriceAndColor(company))
    '''def testTickerRecord(self):
        testAddToTickerRecord('gaggle',15,testTime=time.time() + 60)
        testAddToTickerRecord('rexxon', 1, testTime=time.time() + 100)
        testAddToTickerRecord('rexxon', 1, testTime=time.time() + 60)
        testAddToTickerRecord('rexxon', 1, testTime=time.time() + 100)
        testAddToTickerRecord('rexxon', 1, testTime=time.time() + 150)
        print(getAllTickerRecords())'''
    def testMaxUnitFluctuation(self):
        currentTicker = getTickerPrice('gaggle')
        maxUnitFluctuation = percentValue(currentTicker, game.getMaxUnitFluctuationPercent())
