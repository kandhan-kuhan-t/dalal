import market.orderBook.db as OBDB
import market.orderBook.main as OB
from gameConfig import main as game
from decimal import *

getcontext().prec = 5
def hello():
    return "hello from market, may you win big!"

def newLimitOrder(type,company,limit,units,senderType,userId=''):
    if type == None or limit == None or units == None or senderType == None:
        return -1
    if senderType == 'human':
        return OB.newOrder(type=type,company=company,limit=float(limit),units=int(units),senderType='human',userId=userId)
    elif senderType == 'bot':
        return OB.newOrder(type=type,company=company,limit=float(limit),units=int(units),senderType='bot')

def cancelOrder(id, userId, company, type):
    if type != None or id != None or company != None or userId != None:
        return OB.removeOrder(id=id,userId=userId,company=company,type=type,senderType='human',category='human')
    return -1

def allOrders(company):
    if company not in game.companies():
        return -1
    return OB.getOrderBooks(company=company)

def cancelAllBotOrders(company):
    OB.cancelAllBotOrders(company=company)
    return 1

def initializeCompanies():
    OB.initializeCompanies(game.companies())
    return 1

def setTest(test):
    OB.setTest(test)

def getSpread():
    obj = {}
    for company in game.companies():
        ob = OB.getOrderBooks(company=company)
        botMinimumSellArray = ob['bot']['sell']
        if len(botMinimumSellArray) > 0:
            minimumSell = Decimal(botMinimumSellArray[0]['limit'])/1
        else:
            minimumSell = 'None'
        botMaximumBuyArray = ob['bot']['buy']
        if len(botMaximumBuyArray) > 0:
            maximumBuy = Decimal(botMaximumBuyArray[0]['limit'])/1
        else:
            maximumBuy = 'None'
        print({'minimumSell': minimumSell, 'maximumBuy': maximumBuy})
        obj[company] = {'minimumSell': minimumSell, 'maximumBuy': maximumBuy}
    return obj


if __name__ == '__main__':
    setTest(False)
    print(initializeCompanies())
    print(allOrders('gaggle'))
    print(getSpread('gaggle'))