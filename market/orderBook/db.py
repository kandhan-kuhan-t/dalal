import redis
import pickle
import unittest
from time import time


'''
Order Book[Company]:{
    sell:{
        limit
        unit
        time
        id(if human)
    }Sorted ASC by limit,time
    buy:{
        limit
        unit
        time
        id(if human)
    }Sorted DESC by limit,time
'''


humanOB = 'humanOB'
botOB = 'botOB'

def setConnection(test,db=0):
    if test == True:
        r = redis.StrictRedis(host='localhost', port=6379, db=9)
    if test == False:
        r = redis.StrictRedis(host='localhost', port=6379, db=db)
    return r

def wipeClean(r):
    r.flushdb()

def dump(obj):
    return pickle.dumps(obj)

def load(obj):
    return pickle.loads(obj)

def initializeCompanies(companies, r):
    obj = {
        'sell':[],
        'buy':[]
    }
    for company in companies:
        r.hset(botOB, company, dump(obj))
        r.hset(humanOB, company, dump(obj))
    return 1

def getFreshOne():
    obj = {
        'sell': [],
        'buy': []
    }
    return obj

def getOrderBooks(company, r, only='default'):
    bot = r.hget(botOB, company)
    human = r.hget(humanOB, company)
    if only == 'bot':
        return load(bot)
    elif only == 'human':
        return load(human)
    return {'bot': load(bot), 'human': load(human)}

def setOrderBooks(r, company, botOrderBook={}, humanOrderBook={}, only='default'):
    if only == 'default':
        r.hset(botOB, company, dump(botOrderBook))
        r.hset(humanOB, company, dump(humanOrderBook))
        return 1
    elif only == 'bot':
        r.hset(botOB, company, dump(botOrderBook))
        return 1
    elif only == 'human':
        r.hset(humanOB, company, dump(humanOrderBook))
        return 1
    return -1

def minus(category, type, stockSymbol, units,r):
    book = getOrderBooks(company=stockSymbol, only=category,r=r)
    if type == 'buy':
        book['buy'][0]['units'] -= units
        if book['buy'][0]['units'] == 0:
            book['buy'].pop(0)
    elif type == 'sell':
        book['sell'][0]['units'] -= units
        if book['sell'][0]['units'] == 0:
            book['sell'].pop(0)
    if category == 'human':
        setOrderBooks(company=stockSymbol, humanOrderBook=book, only='human',r=r)
        return 1
    elif category == 'bot':
        setOrderBooks(company=stockSymbol, botOrderBook=book, only='bot', r=r)
        return 1


if __name__ == '__main__':
    r = setConnection(test=False)
    initializeCompanies(['gaggle'], r)
    (getOrderBooks('gaggle',r))

class test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        global r
        r = setConnection(test=True)
        wipeClean(r)

    def testInitializeCompany(self):
        self.assertEqual(initializeCompanies(['gaggle'], r), 1)

    def testSetOrderBooksAndGetOrderBooksBuy(self):
        buyOrderFromHuman = {
            'limit':250,
            'units':100,
            'id':'123-sjjk',
            'time': time()
        }
        arrX = [];arrX.append(buyOrderFromHuman)
        humanOB = {
            'buy': arrX,
            'sell': {}
        }
        buyOrderFromBot = {
            'limit': 250,
            'units': 100,
            'time': time()
        }
        arrY = [];arrY.append(buyOrderFromBot)
        botOB = {
            'buy':arrY,
            'sell':{}
        }
        self.assertEqual(setOrderBooks(company='gaggle',humanOrderBook=humanOB,botOrderBook=botOB,r=r),1)
        #print(getOrderBooks('gaggle',r=r))
        def testMinusPartial(self=self):
            print('Partial Execution')
            print('before minus - HumanOrderBook.buy =>  {}'.format(getOrderBooks('gaggle',r=r,only='human')))
            self.assertEqual(minus(category='human', type='buy', stockSymbol='gaggle', units=80, r=r), 1)
            print('after minus - HumanrOrderBook.buy => {}'.format(getOrderBooks('gaggle',r=r,only='human')))
        def testMinusWhole(self=self):
            print('Whole Execution')
            print('before minus - HumanOrderBook.buy =>  {}'.format(getOrderBooks('gaggle', r=r, only='human')))
            self.assertEqual(minus(category='human', type='buy', stockSymbol='gaggle', units=20, r=r), 1)
            print('after minus - HumanrOrderBook.buy => {}'.format(getOrderBooks('gaggle', r=r, only='human')))
        testMinusPartial()
        testMinusWhole()
    #@unittest.skip('just')1
    def testSetOrderBooksAndGetOrderBooksSell(self):
        sellOrderFromHuman = {
            'limit':250,
            'units':100,
            'id':'123-sjjk',
            'time': time()
        }
        arrX = [];arrX.append(sellOrderFromHuman)
        humanOB = {
            'sell': arrX,
            'buy': []
        }
        sellOrderFromBot = {
            'limit': 250,
            'units': 100,
            'time': time()
        }
        arrY = [];arrY.append(sellOrderFromBot)
        botOB = {
            'sell':arrY,
            'buy':[]
        }
        self.assertEqual(setOrderBooks(company='gaggle',humanOrderBook=humanOB,botOrderBook=botOB,r=r),1)
        #print(getOrderBooks('gaggle',r=r))
        def testMinusPartial(self=self):
            print('Partial Execution')
            print('before minus - HumanOrderBook.sell =>  {}'.format(getOrderBooks('gaggle', r=r, only='human')))
            self.assertEqual(minus(category='human', type='sell', stockSymbol='gaggle', units=80, r=r),1)
            print('after minus - HumanrOrderBook.sell => {}'.format(getOrderBooks('gaggle', r=r, only='human')))
        def testMinusWhole(self=self):
            print('Whole Execution')
            print('before minus - HumanOrderBook.sell =>  {}'.format(getOrderBooks('gaggle', r=r, only='human')))
            self.assertEqual(minus(category='human', type='sell', stockSymbol='gaggle', units=20, r=r), 1)
            print('after minus - HumanrOrderBook.sell => {}'.format(getOrderBooks('gaggle', r=r, only='human')))
        testMinusPartial()
        testMinusWhole()










