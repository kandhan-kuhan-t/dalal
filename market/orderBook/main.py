import market.orderBook.db as OBook
from user import main as User
import unittest
from time import time
from operator import itemgetter,attrgetter
import market.orderBook.util as u
from gameConfig import main as gConf
import market.ticker.main as Ticker
from order import main as OrderDB

ORDER_ADDED_TO_OB = 2
ORDER_IS_EXECUTED = -7
ORDER_CANCELLED_INSUFFICIENT_FUNDS = 3
ERROR_IN_ORDER_EXECUTION = -4
ORDER_CANCELLED_INSUFFICIENT_STOCKS = -8
ORDER_PARTIALLY_EXECUTED_AND_ADDED_TO_OB = -10
MARKET_ORDER_PARTIALLY_EXECUTED_DT_INSUFF_FUNDS = -11

OrderDB.initialize()
TEST = False
USERDB = 1

def setTest(test):
    global TEST,USERDB
    TEST = test
    if test == True:
        USERDB = 10
    global USERDBCONN
    USERDBCONN = User.setConnection(db=USERDB)
    global r
    r = OBook.setConnection(test=test)


'''
Order Book[Company]:{
    sell:{
        limit
        unit
        time
        id(if human)
    }Sorted ASC by limit,time
    buy:{
        limit
        unit
        time
        id(if human)
    }Sorted DESC by limit,time
'''


def executeOrder(type,  userId, units, price, company, senderType, againstOrder, limit):
    amount = units * price
    uId = u.getUserId(senderType=senderType,againstOrder=againstOrder,userId=userId)
    if (senderType == 'human' and type == 'sell') or (
                    senderType == 'bot' and type == 'buy'):  # Condition Checked Previously
        '''User should be credited'''
        if User.credit(userId=uId, amount=amount, r=USERDBCONN) == 1:
            OBook.minus(category=u.against(senderType), type=u.against(type), stockSymbol=company, units=units, r=r)
            Ticker.updateTicker(company=company, executedPrice=price,units=units)
            return ORDER_IS_EXECUTED
        else:
            return ERROR_IN_ORDER_EXECUTION
    elif (senderType == 'human' and type == 'buy') or (
                    senderType == 'bot' and type == 'sell'):  # Here User might not have enough cash, check it before proceeding
        '''User should be credited for due amount'''
        if senderType == 'human':
            debited = units * limit
        else:
            debited = againstOrder['limit'] * units
        due = debited - (units * price)
        if User.creditDueAndIncreaseStock(userId=uId, due=due, stockSymbol=company, units=units, r=USERDBCONN) == 1:
            OBook.minus(category=u.against(senderType), type=u.against(type), stockSymbol=company, units=units, r=r)
            Ticker.updateTicker(company=company, executedPrice=price,units=units)
            return ORDER_IS_EXECUTED
        else:
            return ERROR_IN_ORDER_EXECUTION




def addToOrderBook(type, orderBook, order):
    orderBook.append(order)
    resultSecondary = sorted(orderBook, key=itemgetter('time'), reverse=True) # Sort By Time Descendingly
    if type == 'sell':
        resultPrimary =  sorted(resultSecondary, key=itemgetter('limit'))
    else:
        resultPrimary = sorted(resultSecondary, key=itemgetter('limit'),reverse=True)
    return resultPrimary

def removeFromOrderBook(orderBook, order,type):
    newOrderBookType = [x for x in orderBook[type] if x.get('id') != order['id']]
    secondarySort = sorted(newOrderBookType, key=itemgetter('time'), reverse=True)
    if type == 'sell':
        primarySort =  sorted(secondarySort, key=itemgetter('limit'))
    else:
        primarySort = sorted(secondarySort, key=itemgetter('limit'),reverse=True)
    return primarySort

def getOrderBooks(company):
    return OBook.getOrderBooks(company=company,r=r)

def initializeCompanies(companies):
    OBook.initializeCompanies(companies=companies,r=r)

def matchAndExecute(userId, type, limit, units, company, humanOB, botOB, senderType,orderId):
    book = {'bot':botOB,'human':humanOB}
    if (len(book[u.against(senderType)][u.against(type)])) == 0:
        return units
    else:
        if type == 'sell':
            sellLimit = limit
            buyLimit = u.getLimit(type=u.against(type), ob=book[u.against(senderType)])
        else:
            buyLimit = limit
            sellLimit = u.getLimit(type=u.against(type), ob=book[u.against(senderType)])
        if u.checkLimit(sellOrderLimit=sellLimit, buyOrderLimit=buyLimit) == True:
            maxUnits = min(units, u.getUnits(u.against(type), ob=book[u.against(senderType)]))
            if maxUnits == units:
                price = u.computeStockPrice(sellLimit=sellLimit,
                                            buyLimit=buyLimit)
                againstOrder=book[u.against(senderType)][u.against(type)][0]
                if senderType == 'human':
                    humanUnits = units
                    humanOrderId = orderId
                    if humanUnits == maxUnits:
                        status='executed'
                    else:
                        status='partially executed'
                    User.updateUserOrder(company=company, units=maxUnits, status=status, orderId=humanOrderId, time=time(),
                                         type=type,executedPrice=price, userId=userId, r=USERDBCONN)
                else:
                    humanUnits = book['human'][u.against(type)][0]['units']
                    humanOrderId = book['human'][u.against(type)][0]['id']
                    if humanUnits == maxUnits:
                        status='executed'
                    else:
                        status='partially executed'
                    User.updateUserOrder(company=company, units=maxUnits, status=status, orderId=humanOrderId, time=time(),
                                         type=u.against(type),executedPrice=price, userId=againstOrder['userId'], r=USERDBCONN)

                return executeOrder(type, userId=userId, units=maxUnits, price=price, company=company,
                                    senderType=senderType,
                                    againstOrder=againstOrder, limit=limit)
            else:
                price = u.computeStockPrice(sellLimit=sellLimit,
                                            buyLimit=buyLimit)
                againstOrder = book[u.against(senderType)][u.against(type)][0]
                if senderType == 'human':
                    humanUnits = units
                    humanOrderId = orderId
                    if humanUnits == maxUnits:
                        status = 'executed'
                    else:
                        status = 'partially executed'
                    User.updateUserOrder(company=company, units=maxUnits, status=status, orderId=humanOrderId, time=time(),
                                         type=type,executedPrice=price, userId=userId, r=USERDBCONN)
                else:
                    humanUnits = book['human'][u.against(type)][0]['units']
                    humanOrderId = book['human'][u.against(type)][0]['id']
                    if humanUnits == maxUnits:
                        status = 'executed'
                    else:
                        status = 'partially executed'
                    User.updateUserOrder(company=company, units=maxUnits, status=status, orderId=humanOrderId, time=time(),
                                         type=u.against(type),executedPrice=price, userId=againstOrder['userId'], r=USERDBCONN)

                executeOrder(type, userId=userId, units=maxUnits, price=price, company=company,
                             senderType=senderType, againstOrder=againstOrder,
                             limit=limit)

                return matchAndExecute(userId, type, limit, units - maxUnits, company,
                                       humanOB=OBook.getOrderBooks(company=company, r=r, only='human'),
                                       botOB=OBook.getOrderBooks(company=company, r=r, only='bot'),
                                       senderType=senderType,orderId=orderId)
        else:
            return units

def addAndUpdateOrderBook(orderBooks, senderType, type,company,order, r):
    if senderType == 'human':
        orderBooks[senderType][type] = addToOrderBook(type=type, orderBook=orderBooks[senderType][type],
                                                  order=order)
        OBook.setOrderBooks(company=company, humanOrderBook=orderBooks[senderType], r=r, only=senderType)
        return ORDER_ADDED_TO_OB
    else:
        orderBooks[senderType][type] = addToOrderBook(type=type, orderBook=orderBooks[senderType][type],
                                                      order=order)
        OBook.setOrderBooks(company=company, botOrderBook=orderBooks[senderType], r=r, only=senderType)
        return ORDER_ADDED_TO_OB


def newOrder(type, company, limit, units, senderType, userId=''):
    print('new Limit Order: type-{}, company-{}, limit-{}, units-{}, senderType-{}'.format(type, company, limit, units, senderType))
    orderId = u.generateId()
    orderTime = time()
    OrderDB.set(u.formOrderForOrderDB(limit=limit,units=units,senderType=senderType,id=orderId,time=orderTime,company=company))
    orderBooks = OBook.getOrderBooks(company, r=r)
    if senderType == 'human' and type == 'sell':
        user = User.getUser(userId,r=USERDBCONN)
        availableStock = user['stock'][company]
        if availableStock < units:
            User.updateUserOrder(company=company, units=units, status='Order cancelled due to insufficient stocks', orderId=orderId, time=orderTime,
                                 type=type, userId=userId, r=USERDBCONN)
            return ORDER_CANCELLED_INSUFFICIENT_STOCKS
        else:
            User.decreaseStock(units=units,company=company,userId=userId,r=USERDBCONN)
    if senderType == 'human' and type == 'buy':
        amount = limit * units
        if User.debitMaxLimit(userId=userId, amount=amount, r=USERDBCONN) != 1:
            User.updateUserOrder(company=company, units=units, status='Order cancelled due to insufficient funds',
                                 orderId=orderId, time=orderTime,
                                 type=type, userId=userId, r=USERDBCONN)
            return ORDER_CANCELLED_INSUFFICIENT_FUNDS
    unitsAfterExecutionIfAny = matchAndExecute(type=type, limit=limit, units=units,
                                               senderType=senderType, humanOB=orderBooks['human'],
                                               botOB=orderBooks['bot'], company=company, userId=userId, orderId = orderId)
    if unitsAfterExecutionIfAny == ORDER_IS_EXECUTED:
        return ORDER_IS_EXECUTED
    else:
        '''Add to Appropiate Order Book Buy/Sell'''
        order = u.formOrder(limit=limit,units=unitsAfterExecutionIfAny,time=orderTime,id=orderId)
        if senderType == 'human':
            order['userId'] = userId
        if addAndUpdateOrderBook(orderBooks=orderBooks, senderType=senderType, type=type, company=company,
                                         order=order, r=r) == ORDER_ADDED_TO_OB:
            if units == unitsAfterExecutionIfAny:
                if senderType == 'human':
                    User.updateUserOrder(company=company,units=units,status='added',orderId=orderId,time=orderTime,type=type,userId=userId,r=USERDBCONN)
                return ORDER_ADDED_TO_OB
            else:
                return ORDER_PARTIALLY_EXECUTED_AND_ADDED_TO_OB
    return 0

def removeOrder(id,type,category,company,senderType,userId=''):
    book = OBook.getOrderBooks(company=company, r=r, only=category)
    order = list(filter(lambda x: x['id'] == id, book[type]))
    if len(order) == 0:
       if type == 'buy':

            ordersData = User.getUser(userId=userId, r=USERDBCONN)['orders']
            filtered = list(filter(lambda x: x['orderId'] == id, ordersData))[0]
            units = filtered['totalUnits'] - filtered['units']
            limit = filtered['limit']
            User.updateUserOrder(company=company, units=units, status="cancelled by user", orderId=id,
                                 time=time(),
                                 type=type, userId=userId, r=USERDBCONN,totalUnits=filtered['totalUnits'],limit=limit)
            amount = units * limit
            User.credit(amount=amount, userId=userId, r=USERDBCONN)
       else:
            ordersData = User.getUser(userId=userId, r=USERDBCONN)['orders']
            filtered = list(filter(lambda x: x['orderId'] == id, ordersData))[0]
            units = filtered['totalUnits'] - filtered['units']
            limit = filtered['limit']
            User.updateUserOrder(company=company, units=units, status="cancelled by user", orderId=id,
                                 time=time(),
                                 type=type, userId=userId, r=USERDBCONN,totalUnits=filtered['totalUnits'],limit=limit)

            User.increaseStock(units=units, userId=userId, company=company, r=USERDBCONN)
       return 1
    else:
        order = order[0]
        if senderType == 'human':
            User.updateUserOrder(company=company, units=order['units'], status="cancelled by user", orderId=order['id'],
                                 time=time(),
                                 type=type, userId=userId, r=USERDBCONN)
            if type == 'buy':
                amount = order['limit'] * order['units']
                User.credit(amount=amount, userId=userId, r=USERDBCONN)
                book[type] = removeFromOrderBook(orderBook=book, order=order, type=type)
                OBook.setOrderBooks(company=company, humanOrderBook=book, only='human', r=r)
                return 1
            else:
                units = order['units']
                User.increaseStock(units=units, userId=userId, company=company, r=USERDBCONN)
                book[type] = removeFromOrderBook(orderBook=book, order=order, type=type)
                OBook.setOrderBooks(company=company, humanOrderBook=book, only='human', r=r)
                return 1
        else:
            book[type] = removeFromOrderBook(orderBook=book, order=order, type=type)
            OBook.setOrderBooks(company=company, botOrderBook=book, only='bot',r=r)
            return 1

def removeOrderTest(id,type,category,company,senderType,userId=''):
    book = OBook.getOrderBooks(company=company, r=r, only=category)
    order = list(filter(lambda x: x['id'] == id, book[type]))
    if type == 'buy':
        ordersData = User.getUser(userId=userId, r=USERDBCONN)['orders']
        filtered = list(filter(lambda x: x['orderId'] == id, ordersData))[0]
        units = filtered['totalUnits'] - filtered['units']
        limit = filtered['limit']
        User.updateUserOrder(company=company, units=units, status="cancelled by user", orderId=id,
                         time=time(),
                         type=type, userId=userId, r=USERDBCONN)
        amount = units * limit
        User.credit(amount=amount, userId=userId, r=USERDBCONN)
    else:
        ordersData = User.getUser(userId=userId, r=USERDBCONN)['orders']
        filtered = list(filter(lambda x: x['orderId'] == id, ordersData))[0]
        units = filtered['totalUnits'] - filtered['units']
        limit = filtered['limit']
        User.updateUserOrder(company=company, units=units, status="cancelled by user", orderId=id,
                             time=time(),
                             type=type, userId=userId, r=USERDBCONN)
        User.increaseStock(units=units, userId=userId, company=company, r=USERDBCONN)

    return 1


def cancelAllBotOrders(company):
    orderBook = OBook.getFreshOne()
    OBook.setOrderBooks(company=company, botOrderBook=orderBook, only='bot', r=r)
    return 1


class test(unittest.TestCase):
    #@classmethod
    def setUp(cls):
        setTest(True)
        u.wipeClean(r)
        User.wipeClean(r=USERDBCONN)
        OBook.initializeCompanies(['gaggle'],r=r)
    def testHumanSellBotBuy(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user,r=USERDBCONN)
        userId = User.login(user['username'], user['password'],r=USERDBCONN)
        print(userId)
        '''self.assertEqual(newOrder(type='buy', company='gaggle', units=10, limit=16, senderType='human', userId=userId),
                         2)
        self.assertEqual(newOrder(type='sell', company='gaggle', units=10, limit=16, senderType='bot', userId=userId),
                         ORDER_IS_EXECUTED)
        self.assertEqual(newOrder(type='sell', company='gaggle', units=10, limit=16, senderType='human', userId=userId),
                         2)
        self.assertEqual(newOrder(type='buy', company='gaggle', units=10, limit=16, senderType='bot', userId=userId),
                         ORDER_IS_EXECUTED)'''
        print(User.getUser(userId=userId,r=USERDBCONN))
        self.assertEqual(newOrder(type='buy', company='gaggle', units=10, limit=16, senderType='human', userId=userId),
                         2)
        self.assertEqual(round(User.getUser(userId, r=USERDBCONN)['cash']),840+9000)
        self.assertEqual(newOrder(type='sell', company='gaggle', units=5, limit=12, senderType='bot'),
                         ORDER_IS_EXECUTED)
        print(User.getUser(userId=userId, r=USERDBCONN))
        self.assertEqual(round(User.getUser(userId, r=USERDBCONN)['cash']), 850+9000)
        self.assertEqual(newOrder(type='sell', company='gaggle', units=5, limit=12, senderType='bot'),
                         ORDER_IS_EXECUTED)
        self.assertEqual(round(User.getUser(userId, r=USERDBCONN)['cash']), 860+9000)
        self.assertEqual(OBook.getOrderBooks(company='gaggle', r=r)['human']['buy'],[])
        self.assertEqual(newOrder(type='sell', company='gaggle', units=10, limit=16, senderType='human', userId=userId),
                         2)
        self.assertEqual(newOrder(type='buy', company='gaggle', units=1, limit=20, senderType='bot'),ORDER_IS_EXECUTED)
        self.assertEqual(round(User.getUser(userId, r=USERDBCONN)['cash']), 878+9000)
        self.assertEqual(OBook.getOrderBooks(company='gaggle',r=r)['human']['sell'][0]['units'],9)
        print(User.getUser(userId=userId, r=USERDBCONN))

    def testInsufficientFunds(self):
        user = {
            'name': 'kandhan',
            'username': 'x',
            'password': '12345',
            'email': 'k@k.comx',
            'mobile': '9080908090x',
            'kid': '123kx',
        }
        User.createNewUser(user,r=USERDBCONN)
        userId = User.login(user['username'], user['password'],r=USERDBCONN)
        self.assertEqual(newOrder(type='buy', company='gaggle', units=10, limit=1600, senderType='human', userId=userId),
                         ORDER_CANCELLED_INSUFFICIENT_FUNDS)
        self.assertEqual((User.getUser(userId, r=USERDBCONN)['orders'][0]['status']),
                         'Order cancelled due to insufficient funds')

    def testInsufficientStocks(self):
        user = {
            'name': 'kandhan',
            'username': 'x',
            'password': '12345',
            'email': 'k@k.comx',
            'mobile': '9080908090x',
            'kid': '123kx',
        }
        User.createNewUser(user,r=USERDBCONN)
        userId = User.login(user['username'], user['password'],r=USERDBCONN)
        self.assertEqual(newOrder(type='sell', company='gaggle', units=10, limit=1600, senderType='human', userId=userId),
                         ORDER_CANCELLED_INSUFFICIENT_STOCKS)
        self.assertEqual((User.getUser(userId,r=USERDBCONN)['orders'][0]['status']),'Order cancelled due to insufficient stocks')

    def testRecursiveOrderExecution(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user,r=USERDBCONN)
        userId = User.login(user['username'], user['password'],r=USERDBCONN)
        print(userId)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='bot', userId=userId),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='buy', company='gaggle', units=10, limit=10, senderType='human', userId=userId),
            ORDER_IS_EXECUTED)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=5, limit=10, senderType='human', userId=userId),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='buy', company='gaggle', units=10, limit=10, senderType='bot', userId=userId),
            ORDER_PARTIALLY_EXECUTED_AND_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=5, limit=10, senderType='human', userId=userId),
            ORDER_IS_EXECUTED)

        print(User.getUser(userId,r=USERDBCONN))
        print(OBook.getOrderBooks('gaggle',r=r))
        print(OrderDB.get())

    def testRemoveOrderBot(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        order = newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='bot', userId=userId)
        self.assertEqual(order, ORDER_ADDED_TO_OB)
        self.assertEqual(len(OBook.getOrderBooks(company='gaggle', r=r,only='bot')['sell']),1)
        orderId = OBook.getOrderBooks('gaggle',r=r,only='bot')['sell'][0]['id']
        self.assertEqual(removeOrder(id=orderId,type='sell',category='bot',userId='',company='gaggle',senderType='bot'),1)
        self.assertEqual(OBook.getOrderBooks(company='gaggle',r=r,only='bot')['sell'],[])

    def testRemoveOrderHumanSell(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='bot', userId=userId),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='buy', company='gaggle', units=10, limit=10, senderType='human', userId=userId),
            ORDER_IS_EXECUTED)
        order = newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='human', userId=userId)
        self.assertEqual(order, ORDER_ADDED_TO_OB)
        self.assertEqual(len(OBook.getOrderBooks(company='gaggle', r=r,only='human')['sell']),1)
        orderId = OBook.getOrderBooks('gaggle',r=r,only='human')['sell'][0]['id']
        print(OBook.getOrderBooks(company='gaggle',only='human',r=r))
        self.assertEqual(removeOrder(id=orderId,type='sell',category='human',userId=userId,company='gaggle',senderType='human'),1)
        print(OBook.getOrderBooks(company='gaggle', only='human', r=r))
        self.assertEqual(OBook.getOrderBooks(company='gaggle',r=r,only='human')['sell'],[])
        print(User.getUser(userId=userId,r=USERDBCONN))

    def testRemoveOrderHumanBuy2(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        self.assertEqual(
            newOrder(type='buy', company='gaggle', units=10, limit=10, senderType='human', userId=userId),
            ORDER_ADDED_TO_OB)
        print(User.getUser(userId=userId,r=USERDBCONN)['cash'])
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=6, limit=10, senderType='bot', userId=userId),
            ORDER_IS_EXECUTED)
        orderId = User.getUser(userId=userId,r=USERDBCONN)['orders'][0]['orderId']
        removeOrder(id=orderId,type='buy',category='human',company='gaggle',senderType='human',userId=userId)
        print(User.getUser(userId=userId, r=USERDBCONN)['cash'])

    def testRemoveOrderHumanBuy(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        order = newOrder(type='buy', company='gaggle', units=10, limit=10, senderType='human', userId=userId)
        self.assertEqual(order, ORDER_ADDED_TO_OB)
        self.assertEqual(len(OBook.getOrderBooks(company='gaggle', r=r,only='human')['buy']),1)
        orderId = OBook.getOrderBooks('gaggle',r=r,only='human')['buy'][0]['id']
        print(OBook.getOrderBooks(company='gaggle',only='human',r=r))
        self.assertEqual(removeOrder(id=orderId,type='buy',category='human',userId=userId,company='gaggle',senderType='human'),1)
        print(OBook.getOrderBooks(company='gaggle', only='human', r=r))
        self.assertEqual(OBook.getOrderBooks(company='gaggle',r=r,only='human')['buy'],[])
        print(User.getUser(userId=userId,r=USERDBCONN))

    def testOrderOfOrder(self):
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=20, senderType='bot'),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='bot'),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=30, senderType='bot'),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=15, senderType='bot'),
            ORDER_ADDED_TO_OB)
        book = OBook.getOrderBooks(company='gaggle',r=r,only='bot')['sell']
        self.assertEqual(len(book), 4)
        self.assertEqual(book[0]['limit'],10)
        self.assertEqual(book[1]['limit'], 15)
        self.assertEqual(book[2]['limit'], 20)
        self.assertEqual(book[3]['limit'], 30)
        limit20OrderId = book[2]['id']
        self.assertEqual(removeOrder(id=limit20OrderId,type='sell',category='bot',company='gaggle',senderType='bot'),1)
        book = OBook.getOrderBooks(company='gaggle', r=r, only='bot')['sell']
        self.assertEqual(len(book),3)
        self.assertEqual(book[0]['limit'], 10)
        self.assertEqual(book[1]['limit'], 15)
        self.assertEqual(book[2]['limit'], 30)

    def testCancelBotOrder(self):
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=20, senderType='bot'),
            ORDER_ADDED_TO_OB)
        self.assertEqual(
            newOrder(type='sell', company='gaggle', units=10, limit=10, senderType='bot'),
            ORDER_ADDED_TO_OB)
        book = OBook.getOrderBooks(company='gaggle', r=r, only='bot')['sell']
        self.assertEqual(len(book), 2)
        cancelAllBotOrders('gaggle')
        book = OBook.getOrderBooks(company='gaggle', r=r, only='bot')['sell']
        self.assertEqual(len(book), 0)

    def testPartialExecution(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        order = newOrder(type='sell', company='gaggle', units=1, limit=10, senderType='bot')
        order = newOrder(type='buy', company='gaggle', units=2, limit=10, senderType='human', userId=userId)
        print(User.getUser(userId=userId, r=USERDBCONN))
        orderId = OBook.getOrderBooks(company='gaggle', r=r, only='human')['buy'][0]['id']
        print(removeOrderTest(id=orderId, type='buy', category='human', company='gaggle', senderType='human', userId=userId))
        print(User.getUser(userId=userId, r=USERDBCONN))
    def testRemoveOld(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        order = newOrder(type='sell', company='gaggle', units=1, limit=10, senderType='bot')
        order = newOrder(type='buy', company='gaggle', units=2, limit=10, senderType='human', userId=userId)
        print(User.getUser(userId=userId, r=USERDBCONN))
        print('ORDER BOOK - {}'.format(OBook.getOrderBooks(company='gaggle', r=r, only='human')))
        print('ORDER BOOK - {}'.format(OBook.getOrderBooks(company='gaggle', r=r, only='bot')))
        orderId = OBook.getOrderBooks(company='gaggle', r=r, only='human')['buy'][0]['id']
        print(removeOrder(id=orderId, type='buy', category='human', company='gaggle', senderType='human',
                              userId=userId))
        print(User.getUser(userId=userId, r=USERDBCONN))

    def testErrorCase(self):
        user = {
            'name': 'kandhan',
            'username': 'kkandhaaaaaan',
            'password': '12345',
            'email': 'k@k.com',
            'mobile': '9080908090',
            'kid': '123k',
        }
        User.createNewUser(user, r=USERDBCONN)
        userId = User.login(user['username'], user['password'], r=USERDBCONN)
        order = newOrder(type='sell', company='gaggle', units=1, limit=10, senderType='bot')
        order = newOrder(type='buy', company='gaggle', units=2, limit=10, senderType='human', userId=userId)
        print(User.getUser(userId=userId, r=USERDBCONN))
        orderId = OBook.getOrderBooks(company='gaggle', r=r, only='human')['buy'][0]['id']
        OBook.setOrderBooks(r=r,company='gaggle',humanOrderBook={'sell':[],'buy':[]})
        print(OBook.getOrderBooks(company='gaggle', r=r, only='human'))
        print(removeOrder(id=orderId, type='buy', category='human', company='gaggle', senderType='human',
                          userId=userId))
        print(User.getUser(userId=userId, r=USERDBCONN))



