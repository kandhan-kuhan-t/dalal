import uuid

def wipeClean(r):
    r.flushdb()

def spread(company):
    return 0

def checkLimit(sellOrderLimit, buyOrderLimit):
        return buyOrderLimit >= sellOrderLimit


def computeStockPrice(sellLimit, buyLimit):
    return (sellLimit+buyLimit)/2

def computeStockPriceMarket(ob,type):
    return ob[type][0]['limit']

def getLimit (type,ob):
    return ob[type][0]['limit']

def getUnits (type,ob):
    return ob[type][0]['units']

def against(type):
    if  type == 'human':
        return 'bot'
    elif type == 'bot':
        return 'human'
    elif type == 'sell':
        return 'buy'
    elif type == 'buy':
        return 'sell'

def generateId():
    return str(uuid.uuid1())

def formOrder(limit,units,time,id):
    obj = {
            'limit': limit,
            'units': units,
            'time': time,
            'id':id
    }
    return obj

def formOrderForOrderDB(limit,units,time,id,company,senderType):
    obj = {
            'limit': limit,
            'units': units,
            'time': time,
            'id':id,
            'company':company,
            'senderType':senderType
    }
    return obj


def getUserId(senderType,againstOrder,userId=''):
    if senderType == 'human':
        uId = userId
    else:
        uId = againstOrder['userId']
    return uId

def computeDue():
    return 0