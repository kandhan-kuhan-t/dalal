import redis
from gameConfig import main as game
import pickle

r = redis.StrictRedis(host='localhost',port=6379,db=game.getTickerPort())

def set(obj):
    old = pickle.loads(r.get('order'))
    old.append(obj)
    r.set('order', pickle.dumps(old))
    return

def get():
    x = r.get('order')
    if x == None:
        return []
    else:
        return pickle.loads(x)

def getById(id):
    x = get()
    filtered = list(filter((lambda y: y['id'] == id), x))
    print('backend filtered {}'.format(filtered))
    if len(filtered) > 0:
        return filtered[0]
    else:
        return None

def initialize():
    r.set('order',pickle.dumps([]))
    return

if __name__ == '__main__':
    print(get())